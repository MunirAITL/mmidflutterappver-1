import 'dart:async';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/scan/pages/start_page.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'auth/NoAccess.dart';

class SplashPage extends StatefulWidget {
  @override
  VideoState createState() => VideoState();
}

class VideoState extends State<SplashPage>
    with SingleTickerProviderStateMixin, Mixin {
  bool isDoneCookieCheck = false;

  @override
  void initState() {
    super.initState();

    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      try {
        Future.delayed(Duration(seconds: 3), () async {
//  cookie
          CookieJar cj = await CookieMgr().getCookiee();
          final listCookies =
              await cj.loadForRequest(Uri.parse(Server.BASE_URL));
          if (listCookies.length > 0 &&
              await DBMgr.shared.getTotalRow("User") > 0) {
            await userData.setUserModel();

            if (Server.isTest) {
              if (userData.userModel.communityID.toString() == "1") {
                Get.offAll(
                  () => StartPage(),
                ).then((value) {
                  setState(() {
                    isDoneCookieCheck = true;
                  });
                });
              } else {
                Get.off(() => NoAccess());
              }
            } else if (userData.userModel.isMobileNumberVerified) {
              if (userData.userModel.communityID.toString() == "1") {
                Get.offAll(
                  () => StartPage(),
                ).then((value) {
                  //callback(route);
                });
              } else {
                Get.off(() => NoAccess());
              }
            } else {
              Get.to(() => Sms2Page(mobile: userData.userModel.mobileNumber));
            }
          } else {
            Get.offAll(
              () => StartPage(),
            ).then((value) {
              //callback(route);
            });
          }
        });
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          //width: getHP(context, 50),
          width: getW(context),
          height: getHP(context, 5),
          child: Text(
            "Magic Id",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontSize: 17,
              fontWeight: FontWeight.bold,
            ),
          ), /*new Image.asset(
            'assets/images/logo/logo.png',
            //fit: BoxFit.cover,
          ),*/
        ),
      ),
    );
  }
}
