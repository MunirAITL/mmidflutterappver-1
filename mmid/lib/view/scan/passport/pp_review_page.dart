import 'dart:convert';
import 'dart:io';
import 'package:aitl/config/server/APIMediaCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/media_upload/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/data/model/media_upload/PostDocVerifyAPIModel.dart';
import 'package:aitl/data/model/media_upload/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/scan/pages/scan_id_doc_page.dart';
import 'package:aitl/view/scan/pages/start_page.dart';
import 'package:aitl/view/scan/passport/pp_open_cam_page.dart';
import 'package:aitl/view/scan/passport/pp_result_page.dart';
import 'package:aitl/view/scan/passport/pp_review_base.dart';
import 'package:aitl/view/utils/ui_helper.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mime_type/mime_type.dart';

class PPReviewPage extends StatefulWidget {
  const PPReviewPage({Key key}) : super(key: key);
  @override
  State createState() => _PPReviewPageState();
}

class _PPReviewPageState extends PPReviewBase<PPReviewPage> {
  final list = [
    "Check the following:",
    "* Image is sharp",
    "* There are no reflections",
    "* All personal details are visible"
  ];

  @override
  void initState() {
    animationController = new AnimationController(
        duration: new Duration(seconds: 1), vsync: this);
    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animateScanAnimation(true);
      } else if (status == AnimationStatus.dismissed) {
        animateScanAnimation(false);
      }
    });
    animateScanAnimation(false);
    setState(() {
      isFrontAnim = false;
    });

    super.initState();
  }

  @override
  void dispose() {
    if (animationController != null) animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Prove your identity",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.off(() => ScanIDDocPage());
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Container(
              color: MyTheme.greyColor,
              child: Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawPicBox(),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          UIHelper().drawLine(h: 1),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Review photos of passport",
                              txtColor: Colors.black87,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          drawIssues(list),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 20, left: 20, right: 20, bottom: 10),
              child: (!isFrontAnim)
                  ? MMBtn(
                      txt: "Confirm",
                      bgColor: MyTheme.greenColor,
                      txtColor: Colors.white,
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 0,
                      callback: () async {
                        /*isFrontAnim = true;
                        setState(() {});
                        doFrontOCR(() {
                          isFrontAnim = false;
                          setState(() {});
                        });*/

                        try {
                          String mimeType = mime(appData.file_pp_front.path);
                          var fileName =
                              (appData.file_pp_front.path.split('/').last);
                          //String mimee = mimeType.split('/')[0];
                          String type = mimeType.split('/')[1];
                          List<int> fileInByte =
                              appData.file_pp_front.readAsBytesSync();
                          final imageData = base64Encode(fileInByte);
                          print(imageData);
                          await APIViewModel().req<
                                  PostSelfieByBase64DataAPIModel>(
                              context: context,
                              url: APIMediaCfg.SELFIE_POST_URL,
                              reqType: ReqType.Post,
                              param: {
                                "UserCompanyId":
                                    userData.userModel.userCompanyID,
                                "ImageType": type,
                                "FileName": fileName,
                                "ContentData": imageData,
                              },
                              callback: (model) async {
                                if (model != null && mounted) {
                                  if (model.success) {
                                    await APIViewModel().req<
                                            PostDocByBase64BitAPIModel>(
                                        context: context,
                                        url: APIMediaCfg.DOC_POST_URL,
                                        reqType: ReqType.Post,
                                        headers: {
                                          "Accept": "application/json",
                                          "Content-Type":
                                              "application/x-www-form-urlencoded",
                                        },
                                        param: {
                                          "UserCompanyId":
                                              userData.userModel.userCompanyID,
                                          "ImageType": type,
                                          "FileName": fileName,
                                          "ContentData": imageData,
                                          "DocumentType": "2",
                                          "RequestId": model.responseData
                                              .responsedata.requestid,
                                        },
                                        callback: (model2) async {
                                          if (model2 != null && mounted) {
                                            if (model2.success) {
                                              await APIViewModel()
                                                  .req<PostDocVerifyAPIModel>(
                                                      context: context,
                                                      url: APIMediaCfg
                                                          .VERIFY_POST_URL,
                                                      reqType: ReqType.Post,
                                                      param: {
                                                        "Requestid": model
                                                            .responseData
                                                            .responsedata
                                                            .requestid,
                                                      },
                                                      callback: (model3) {
                                                        if (model3 != null &&
                                                            mounted) {
                                                          if (model3.success) {
                                                            Get.off(() =>
                                                                PPResultPage(
                                                                    model:
                                                                        model3));
                                                          }
                                                        }
                                                      });
                                            }
                                          }
                                        });
                                  }
                                }
                              });
                        } catch (e) {}
                      })
                  : SizedBox(),
            ),
            (!isFrontAnim)
                ? Center(
                    child: TextButton(
                      onPressed: () {
                        appData.file_pp_front = null;
                        Get.back(result: true);
                      },
                      child: Text(
                        "Rescan passport",
                        style: TextStyle(
                          color: Colors.black38,
                          fontSize: 17,
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
