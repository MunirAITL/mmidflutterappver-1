import 'package:aitl/config/cfg/Define.dart';
import 'package:intl/intl.dart';

mixin PassportMixin {
  hasStringOnly(String str) {
    return RegExp('[a-zA-Z]').hasMatch(str);
  }

  getType(String str) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == 'e' &&
          str[i - 1] == 'p' &&
          str[i - 2] == 'y' &&
          str[i - 3] == 'T') {
        break;
      }
      str2 += str[i];
    }
    bool isStr = hasStringOnly(str2);
    return (isStr && str2.length == 1) ? str2 : '';
  }

  getCode(String str) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == 'e' &&
          str[i - 1] == 'd' &&
          str[i - 2] == 'o' &&
          str[i - 3] == 'C') {
        break;
      }
      str2 += str[i];
    }
    str2 = str2.replaceAll(".", "");
    str2 = str2.split('').reversed.join();
    bool isCode = hasStringOnly(str2);
    return {
      'code': isCode ? str2 : null,
      'ppNo': !isCode ? str2 : null,
    };
  }

  getPassportNo(String str) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == 'o' && str[i - 1] == 'N' && str[i - 2] == ' ') {
        break;
      }
      str2 += str[i];
    }
    str2 = str2.replaceAll(".", "");
    str2 = str2.split('').reversed.join();
    bool isCode = hasStringOnly(str2);
    return {
      'code': isCode ? str2 : null,
      'ppNo': !isCode ? str2 : null,
    };
  }

  //  29 MAR /MARS 84
  getPPDate(String str) {
    try {
      try {
        if (!str.contains("/")) {
          final strArr = str.split(" ");
          if (int.parse(strArr[0]) > 0 && int.parse(strArr[3]) > 0) {
            str = strArr[0].trim() +
                ' ' +
                strArr[1].trim() +
                '/' +
                strArr[2].trim() +
                ' ' +
                strArr[3].trim();
            print(str);
          }
        }
      } catch (e) {}

      final strArr = str.split("/");
      final mmArr = strArr[0].split(" ");
      final yyArr = strArr[1].split(" ");
      var mm = capitalize(mmArr[1].toLowerCase());
      String yyyy = yyArr[1];
      String dtOriginal = strArr[0].replaceAll(" ", "-") + yyyy;

      final dtArr = dtOriginal.split("-");
      if (!Define.listMMM.contains(mm)) {
        mm = dtArr[1];
      }

      try {
        int i = 1;
        for (var mmm in Define.listMMM) {
          if (mm.toLowerCase().startsWith(mmm.toLowerCase())) {
            break;
          }
          i++;
        }
        final dt2 = dtArr[0] + "-" + i.toString() + "-" + yyyy;
        final twoDigitYear = DateFormat("dd-MM-yy").parse(dt2).year;
        return strArr[0].replaceAll(" ", "-") + twoDigitYear.toString();
      } catch (e) {
        return "";
      }
    } catch (e) {}
    return "";
  }

  getSex(String str) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == ')' && str[i - 1] == '5' && str[i - 2] == '(') {
        break;
      }
      str2 += str[i];
    }
    bool isStr = hasStringOnly(str2);
    return (isStr && str2.length == 1) ? str2 : '';
  }

  getPlaceBirth(String str) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == ')' && str[i - 1] == '6' && str[i - 2] == '(') {
        break;
      }
      str2 += str[i];
    }
    str2 = str2.split('').reversed.join();
    bool isStr = hasStringOnly(str2);
    return (isStr) ? str2 : '';
  }

  getDateIssue(String str) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == ')' && str[i - 1] == '7' && str[i - 2] == '(') {
        break;
      }
      str2 += str[i];
    }
    return str2;
  }

  getAuthority(String str) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == ')' && str[i - 1] == '8' && str[i - 2] == '(') {
        break;
      }
      str2 += str[i];
    }
    str2 = str2.split('').reversed.join();
    bool isStr = hasStringOnly(str2);
    return (isStr) ? str2 : '';
  }

  getDateExpiry(String str, String e) {
    String str2 = "";
    for (int i = str.length - 1; i > 0; i--) {
      if (str[i] == ')' && str[i - 1] == e && str[i - 2] == '(') {
        break;
      }
      str2 += str[i];
    }
    if (str2.length > 0) {
      return getPPDate(str2);
    }
    return str2;
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
}
