import 'dart:io';
import 'package:aitl/config/cfg/Define.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/view/scan/passport/pp_mixin.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/utils/scan_mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/anim/ImageScannerAnimation.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view_model/rx/gender_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class PPReviewBase<T extends StatefulWidget> extends State<T>
    with
        Mixin,
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin,
        ScanMixin,
        PassportMixin {
  bool isFrontAnim = false;

  AnimationController animationController;

  final ppType = TextEditingController();
  final ppCode = TextEditingController();
  final ppNo = TextEditingController();
  final ppSurName = TextEditingController();
  final ppGivenName = TextEditingController();
  final ppPlaceBirth = TextEditingController();
  final ppAuthority = TextEditingController();

  DropListModel ddMaritalNationalities;
  OptionItem optNationalities;

  GenderController cGender = Get.put(GenderController());

  var dob = "";
  var dtIssue = "";
  var dtExpiry = "";
  String last2lines = "";

  bool isAnim = true;

  drawLayout();

  drawPicBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Stack(
        children: [
          Container(
            width: getW(context),
            height: getHP(context, 30),
            decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: MyTheme.greenColor, width: 1)),
            child: Image.file(
              appData.file_pp_front,
              fit: BoxFit.fill,
            ),
          ),
          (isFrontAnim)
              ? ImageScannerAnimation(
                  !isFrontAnim,
                  getW(context),
                  animation: animationController,
                )
              : SizedBox()
        ],
      ),
    );
  }

  drawIssues(List<String> list) {
    return Container(
      color: MyTheme.l3BlueColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: list.length,
            itemBuilder: (context, index) {
              return Txt(
                  txt: list[index],
                  txtColor: Colors.blue,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false);
            }),
      ),
      //return Txt(txt: list[i], txtColor: Colors.blue, txtSize: MyTheme.txtSize, txtAlign: TextAlign.start, isBold: false)}),
    );
  }

  void animateScanAnimation(bool reverse) {
    if (reverse) {
      animationController.reverse(from: 1.0);
    } else {
      animationController.forward(from: 0.0);
    }
  }

  //  start google vision api
  doFrontOCR(Function callback) async {
    doGoogleVisionOCR(appData.file_pp_front).then((body) {
      if (body != null) {
        log(body);
        parseFrontSide1(body);
        parseFrontSide2(body);
        callback();
      } else {
        showAlert(msg: 'failed to parse passport');
        callback();
      }
    });
  }

  parseFrontSide1(String body) {
    try {
      //  Try with google vision
      //var gbody2 = body.replaceAll(".\n", ".");
      final gbodyArr = body.split("\\n");

      //var gbody2 = body.replaceAll(".\n", ".");
      //final gbodyArr = gbody2.split("\n");
      final ppCode = "GBR";
      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "");
        print(v);

        //  Passport No
        if (ppNo.text.isEmpty && v.startsWith("Pass")) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          if (hasStringOnly(v2)) {
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
            if (hasStringOnly(v2)) {
              txt2 = gbodyArr[i + 3];
              v2 = txt2.replaceAll("\n", "");
            }
            if (!hasStringOnly(v2)) {
              ppNo.text = v2;
            }
          } else {
            if (!hasStringOnly(v2)) {
              ppNo.text = v2;
            }
          }
        }
        //  Surname/Nom
        else if (ppSurName.text.isEmpty && (v.endsWith("1)"))) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          //bool isString = hasStringOnly(v2);
          //if (isString) ppSurName.text = v2;
          if (hasStringOnly(v2)) {
            ppSurName.text = v2;
          }
        }
        //  Given names/Prénoms
        else if (ppGivenName.text.isEmpty &&
            (v.endsWith("2)") || v.contains("(2"))) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          //bool isString = hasStringOnly(v2);
          //if (isString) ppGivenName.text = v2;
          if (hasStringOnly(v2)) {
            ppGivenName.text = v2;
          }
        }
        //  Nationality/Nationalité
        else if (v.endsWith("3)") || v.contains("(3")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          bool isString = hasStringOnly(v2);
          if (isString) {
            if (hasStringOnly(v2)) {
              optNationalities.title = v2;
            }
          }
        }
        //  Date of birth/Date de naissance
        else if (dob == '' && (v.endsWith("4)") || v.contains("(4"))) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          dob = getPPDate(v2);
        }
        //  Sex/Sexe + Place of birth/Lieu de naissance
        else if (v.endsWith("5)") || v.contains("(5")) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          if (v2.contains('M')) cGender.gender = genderEnum.male.obs;
          if (v2.contains('F')) cGender.gender = genderEnum.female.obs;
        } else if (ppPlaceBirth.text.isEmpty &&
            (v.endsWith("6)") || v.contains("(6"))) {
          //  now getting sex and place of birth NOW...
          var txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          if (hasStringOnly(v2) && v2.length < 3) {
            if (v2.contains('M')) cGender.gender = genderEnum.male.obs;
            if (v2.contains('F')) cGender.gender = genderEnum.female.obs;
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
            if (hasStringOnly(v2)) {
              ppPlaceBirth.text = v2;
            }
          } else {
            if (hasStringOnly(v2)) {
              ppPlaceBirth.text = v2;
            }
          }
        }
        //  Date of issue/Date de délivrance + Authority/Autorité
        else if (dtIssue == '' && (v.endsWith("7)") || v.contains("(7"))) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          String dtIssueStr = getPPDate(v2);
          if (dtIssueStr.length > 0) {
            dtIssue = dtIssueStr;
          } else {
            var txt2 = gbodyArr[i + 2];
            var v2 = txt2.replaceAll("\n", "");
            String dtIssueStr = getPPDate(v2);
            if (dtIssueStr.length > 0) {
              dtIssue = dtIssueStr;
            }
          }
        } else if (ppAuthority.text.isEmpty &&
            (v.endsWith("8)") || v.contains("(8"))) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          if (v2.contains("/")) {
            String str = getPPDate(v2);
            if (dtIssue == '' && str.length > 0) {
              dtIssue = str;
            }
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
            ppAuthority.text = v2;
          } else if (hasStringOnly(v2)) {
            ppAuthority.text = v2;
          }
        }
        //  Date of expiry/Date d'expiration
        else if (dtExpiry == '' && (v.endsWith("9)") || v.contains("(9"))) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          String str = getPPDate(v2);
          if (str.length > 0) {
            dtExpiry = str;
          } else {
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
            String str = getPPDate(v2);
            if (str.length > 0) {
              dtExpiry = str;
            }
          }
        }
        //  Holder's signature/Signature du titulaire
        else if (dtExpiry == '' && (v.endsWith("10)") || v.contains("(10"))) {
          String dtExpiryStr = getDateExpiry(v, "10");
          dtExpiryStr = getPPDate(dtExpiryStr);
          if (dtExpiry == '' && dtExpiryStr.length > 0) {
            dtExpiry = dtExpiryStr;
          } else {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            String dtExpiryStr = getPPDate(v2);
            if (dtExpiry == '' && dtExpiryStr.length > 0) {
              dtExpiry = dtExpiryStr;
            }
          }
        } else if (ppType.text.isEmpty && v.contains("u003c")) {
          try {
            final str = v[0];
            ppType.text = str;
          } catch (e) {}
        }
        sleep(Duration(microseconds: 100));
      }
    } catch (e) {}
  }

  parseFrontSide2(String body) {
    try {
      //  Try with google vision
      //var gbody2 = body.replaceAll(".\n", ".");
      final gbodyArr = body.split("\\n");

      //var gbody2 = body.replaceAll(".\n", ".");
      //final gbodyArr = gbody2.split("\n");
      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "");
        print(v);

        //  Surname/Nom
        if (ppSurName.text.isEmpty && v.contains("Nom ") && v.endsWith(")")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          //bool isString = hasStringOnly(v2);
          //if (isString) ppSurName.text = v2;
          if (hasStringOnly(v2)) {
            ppSurName.text = v2;
          }
        }
        //  Given names/Prénoms
        else if (ppGivenName.text.isEmpty &&
            v.startsWith("G") &&
            v.endsWith(")")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          //bool isString = hasStringOnly(v2);
          //if (isString) ppGivenName.text = v2;
          if (hasStringOnly(v2)) {
            ppGivenName.text = v2;
          }
        }
        //  Nationality/Nationalité
        else if (optNationalities.title != 'Nationality' &&
            v.startsWith("N") &&
            v.endsWith(")")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          if (hasStringOnly(v2)) {
            optNationalities.title = v2;
          }
        }
        //  Date of birth/Date de naissance
        else if (dob == '' && v.contains("birth") && v.endsWith(")")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          dob = getPPDate(v2);
        }
        //  Sex/Sexe + Place of birth/Lieu de naissance
        else if (v.endsWith("5)")) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          if (v2.contains('M')) cGender.gender = genderEnum.male.obs;
          if (v2.contains('F')) cGender.gender = genderEnum.female.obs;
        } else if (ppPlaceBirth.text.isEmpty &&
            v.startsWith("P") &&
            v.endsWith(")")) {
          //  now getting sex and place of birth NOW...
          final txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          if (hasStringOnly(v2)) {
            ppPlaceBirth.text = v2;
          }
        }
        //  Date of issue/Date de délivrance + Authority/Autorité
        else if (dtIssue == '' && v.contains("issue") && v.endsWith(")")) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          String dtIssueStr = getPPDate(v2);
          if (dtIssueStr.length > 0) {
            dtIssue = dtIssueStr;
          }
        } else if (ppAuthority.text.isEmpty &&
            (v.startsWith("Authority") && (v.endsWith(")")))) {
          final txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          ppAuthority.text = v2;
        }
        //  Date of expiry/Date d'expiration
        else if (dtExpiry == '' && v.contains("expiry") && v.endsWith(")")) {
          var txt2 = gbodyArr[i + 1];
          var v2 = txt2.replaceAll("\n", "");
          String str = getPPDate(v2);
          if (str.length > 0) {
            dtExpiry = str;
          }
        }
        //  Holder's signature/Signature du titulaire
        else if (dtExpiry == '' && v.contains("signature") && v.endsWith(")")) {
          String dtExpiryStr = getDateExpiry(v, "10");
          dtExpiryStr = getPPDate(dtExpiryStr);
          if (dtExpiry == '' && dtExpiryStr.length > 0) {
            dtExpiry = dtExpiryStr;
          } else {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            String dtExpiryStr = getPPDate(v2);
            if (dtExpiry == '' && dtExpiryStr.length > 0) {
              dtExpiry = dtExpiryStr;
            }
          }
        } else if (ppType.text.isEmpty && v.contains("u003c")) {
          try {
            final str = v[0];
            ppType.text = str;
          } catch (e) {}
        }
        sleep(Duration(microseconds: 100));
      }
    } catch (e) {}
  }
}
