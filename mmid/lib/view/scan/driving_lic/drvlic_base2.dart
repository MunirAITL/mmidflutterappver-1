import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/utils/ui_helper.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class DrvLic2Base<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();

  drawRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10),
            drawCell(
                0,
                Icon(
                  Icons.drive_eta,
                  color: Colors.green,
                  size: 20,
                ),
                "Yes, English with a transparent window",
                true),
            drawCell(
                1,
                Icon(
                  Icons.drive_eta,
                  color: Colors.green,
                  size: 20,
                ),
                "No, not English and/or no window",
                false),
          ],
        ),
      ),
    );
  }

  drawCell(i, ico, title, isLine) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () {
          Get.off(() => OpenCamDrvLicPage());
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: UIHelper().drawLine())
                : SizedBox(height: 20)
          ],
        ),
      ),
    );
  }
}
