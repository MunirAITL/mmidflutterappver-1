import 'dart:io';
import 'package:aitl/config/cfg/Define.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/view/scan/driving_lic/dl_mixin.dart';
import 'package:aitl/view/utils/fun.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/utils/scan_mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/anim/ImageScannerAnimation.dart';
import 'package:flutter/material.dart';

abstract class DrvLicReviewBase<T extends StatefulWidget> extends State<T>
    with
        Mixin,
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin,
        ScanMixin,
        DrivingMixin {
  bool isFrontAnim = false;

  AnimationController animationController;

  drawLayout();

  drawPicBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Stack(
        children: [
          Container(
            width: getW(context),
            height: getHP(context, 30),
            decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: MyTheme.greenColor, width: 1)),
            child: Image.file(
              appData.file_drvlic_front,
              fit: BoxFit.fill,
            ),
          ),
          (isFrontAnim)
              ? ImageScannerAnimation(
                  !isFrontAnim,
                  getW(context),
                  animation: animationController,
                )
              : SizedBox()
        ],
      ),
    );
  }

  drawIssues(List<String> list) {
    return Container(
      color: MyTheme.l3BlueColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: list.length,
            itemBuilder: (context, index) {
              return Txt(
                  txt: list[index],
                  txtColor: Colors.blue,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false);
            }),
      ),
      //return Txt(txt: list[i], txtColor: Colors.blue, txtSize: MyTheme.txtSize, txtAlign: TextAlign.start, isBold: false)}),
    );
  }

  void animateScanAnimation(bool reverse) {
    if (reverse) {
      animationController.reverse(from: 1.0);
    } else {
      animationController.forward(from: 0.0);
    }
  }

  //  start google vision api
  doFrontOCR(Function callback) {
    doGoogleVisionOCR(appData.file_drvlic_front).then((body) {
      if (body != null) {
        log(body);
        parseFrontSide(body);
        callback();
      } else {
        showAlert(msg: 'failed to parse driving licence - front');
        callback();
      }
    });
  }

  parseFrontSide(String gbody) {
    var dob = "";
    try {
      var gbody2 = gbody.replaceAll(".\\n", ".");
      final gbodyArr = gbody2.split("\\n");
      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "").toLowerCase();
        print(v);

        if (v.startsWith("1 ") || v.startsWith("1.")) {
          //givenName.text = trim(v);
          log(Fun.trim(v));
        } else if (v.startsWith("2 ") || v.startsWith("2.")) {
          //familyName.text = Fun.trim(v);
          log(Fun.trim(v));
        } else if (v.startsWith("3 ") || v.startsWith("3.")) {
          final str = Fun.trim(v);
          try {
            final strArr = str.toString().split(" ");
            final dobArr = strArr[0].toString().split(".");
            String dd = dobArr[0];
            String mm = dobArr[1];
            String yy = dobArr[2];
            dob = Fun.trim(dd + '-' + Define.listMMM[int.parse(mm)] + '-' + yy);
            log(dob);
          } catch (e) {}
        } else if (dob == '') {
          final strArr = v.split(".");
          if (strArr.length > 1) {
            try {
              final strArr = v.split(" ");
              final dobArr = strArr[0].split(".");
              String dd = dobArr[0];
              String mm = dobArr[1];
              String yy = dobArr[2];
              dob =
                  Fun.trim(dd + '-' + Define.listMMM[int.parse(mm)] + '-' + yy);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }
}
