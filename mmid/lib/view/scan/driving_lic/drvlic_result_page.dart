import 'dart:convert';

import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/media_upload/PostDocVerifyAPIModel.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DrvLicResultPage extends StatefulWidget {
  final PostDocVerifyAPIModel model;
  const DrvLicResultPage({Key key, @required this.model}) : super(key: key);
  @override
  _DrvLicResultPageState createState() => _DrvLicResultPageState();
}

class _DrvLicResultPageState extends State<DrvLicResultPage> with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Identification of a person",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    final model = widget.model.responseData.response[0];
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            drawPicBox(),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: model.name,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize + 1,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 20),
                  Txt(
                      txt: "Identifiction data",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Birthday: " + model.dateofBirth ?? '',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Personal ID number: " + model.id.toString() ?? '',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Document type: " + model.documentType ?? '',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Identification type: " +
                          ((model.isIdentity)
                              ? 'Identification with liveness'
                              : 'Identification without liveness'),
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Identification status: " + model.status.toString(),
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 20),
                  Txt(
                      txt: "Analysis result: " + "",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Guessed age: " + model.guessedAge.toString(),
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Txt(
                      txt: "Real age: " + model.realAge.toString(),
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawPicBox() {
    final model = widget.model.responseData.response[0];
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: getWP(context, 43),
            height: getHP(context, 15),
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(color: MyTheme.greenColor, width: 1),
              image: DecorationImage(
                image: MemoryImage(base64Decode(model.image1)),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(width: 5),
          Container(
            width: getWP(context, 43),
            height: getHP(context, 15),
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(color: MyTheme.greenColor, width: 1),
              image: DecorationImage(
                image: MemoryImage(base64Decode(model.image2)),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
