import 'dart:async';
import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/scan/pages/scan_id_doc_page.dart';
import 'package:aitl/view/scan/pages/start_page.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'drvlic_open_cam_base.dart';

class OpenCamDrvLicPage extends StatefulWidget {
  const OpenCamDrvLicPage({Key key}) : super(key: key);
  @override
  State createState() {
    return _OpenCamDrvLicPageState();
  }
}

class _OpenCamDrvLicPageState extends OpenCamDrvLicBase<OpenCamDrvLicPage>
    with WidgetsBindingObserver, TickerProviderStateMixin, Mixin {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
  }

  loadCam() async {
    final cameras = await availableCameras();
    controller = CameraController(cameras[0], ResolutionPreset.veryHigh);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    loadCam();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        /*/appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Smart card completed",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),*/
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    if (controller == null) return drawEmpty();
    return controller.value.isInitialized
        ? Stack(children: [
            Positioned.fill(
                child: AspectRatio(
                    aspectRatio: controller.value.aspectRatio,
                    child: CameraPreview(controller))),
            new Positioned.fill(
              child: new Image.asset(
                'assets/images/img/cam_card_bg.png',
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
                top: getHP(context, 1),
                right: 5,
                child: Container(
                  //width: getW(context),
                  child: IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.close,
                        color: MyTheme.greenColor,
                        size: 25,
                      )),
                )),
            Positioned(
                top: getHP(context, 20),
                child: Container(
                  width: getW(context),
                  child: Center(
                    child: Text(
                      "Scan your driving licence - front",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )),
            /*Positioned(
                top: getHP(context, 20),
                child: Container(
                  width: getW(context),
                  child: Center(
                    child: (isFront)
                        ? Text(
                            "Start with the front",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.check,
                                color: Colors.white,
                              ),
                              SizedBox(width: 5),
                              Text("Front"),
                              SizedBox(width: 20),
                              Icon(
                                Icons.close,
                                color: Colors.white,
                              ),
                              SizedBox(width: 5),
                              Text("Back"),
                            ],
                          ),
                  ),
                )),*/

            Positioned(
                top: getHP(context, 75),
                child: /*(isFront)
                    ?*/
                    Container(
                  width: getW(context),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: MyTheme.greenColor),
                  child: Center(
                    child: IconButton(
                        iconSize: 40,
                        icon: const Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                        ),
                        color: Colors.blue,
                        onPressed: onTakePictureButtonPressed),
                  ),
                )
                /*: Container(
                        width: getW(context),
                        child: IconButton(
                            iconSize: 40,
                            onPressed: onTakePictureButtonPressed,
                            icon: Icon(
                              Icons.refresh,
                              color: Colors.white,
                            ))),*/
                ),
          ])
        : drawEmpty();
  }

  drawEmpty() {
    return Container(
      width: getW(context),
      height: getH(context),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/img/smart_card_bg.png"),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
