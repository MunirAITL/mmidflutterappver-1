import 'dart:io';
import 'dart:typed_data';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/view/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/scan/driving_lic/drvlic_review_page.dart';
import 'package:aitl/view/utils/scan_mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:crop_your_image/crop_your_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class CropImageDrvLicPage extends StatefulWidget {
  File file;
  final GlobalKey rectKey;
  final int index;
  CropImageDrvLicPage({
    Key key,
    @required this.file,
    @required this.index,
    @required this.rectKey,
  }) : super(key: key);
  @override
  State createState() => _CropImageDrvLicPageState();
}

class _CropImageDrvLicPageState extends State<CropImageDrvLicPage>
    with ScanMixin, Mixin {
  final _controller = CropController();

  Uint8List list8Data;
  int width, height;

  @override
  void initState() {
    super.initState();
    //  getting image data for cropping
    widget.file.readAsBytes().then((data) {
      if (mounted) {
        list8Data = data;
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    list8Data = null;
    super.dispose();
  }

  /*go2(File file2) {
    EasyLoading.dismiss();
    switch (widget.index) {
      case 0:
        Get.off(() => (widget.isFrontSide)
            ? DrivingFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : DrivingBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      case 1:
        Get.off(() => (widget.isFrontSide)
            ? PassPortFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : PassPortBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      case 2:
        Get.off(() => (widget.isFrontSide)
            ? EUIDCardFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : EUIDCardBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      default:
    }
  }*/

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Crop your driving license",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            (list8Data != null)
                ? IconButton(
                    onPressed: () {
                      appData.file_drvlic_front = null;
                      Get.back(result: true);
                    },
                    icon: Icon(
                      Icons.close,
                      color: MyTheme.greenColor,
                      size: 20,
                    ))
                : SizedBox()
          ],
        ),
        body: (list8Data != null && mounted) ? drawLayout() : SizedBox(),
      ),
    );
  }

  drawLayout() {
    //final box = widget.rectKey.globalPaintBounds;
    //Offset position = box.localToGlobal(Offset.zero);
    return Stack(
      alignment: Alignment.center,
      children: [
        Crop(
            image: list8Data,
            aspectRatio: 1.5,
            //initialArea: box,
            //initialSize: .8,
            cornerDotBuilder: (size, cornerIndex) =>
                const DotControl(color: Colors.green),
            baseColor: MyTheme.bgColor.withOpacity(.5),
            maskColor: MyTheme.bgColor.withOpacity(.5),
            controller: _controller,
            onCropped: (_croppedData) async {
              // do something with image data
              //MemoryImage(_croppedData);
              if (_croppedData != null && mounted) {
                //final dir = await getExternalStorageDirectory();
                //final myImagePath = dir.path + "/myimg.png";
                //var f = File(myImagePath);
                //if (await f.exists()) {
                //await f.delete();
                //}
                /*_croppedData = resizeImage(
                  data: _croppedData,
                  width: ImageCfg.width,
                  height: ImageCfg.height,
                );*/
                //var decodedImage = await decodeImageFromList(_croppedData);
                //width = decodedImage.width;
                //height = decodedImage.height;
                final f = await widget.file.writeAsBytes(_croppedData);
                appData.file_drvlic_front = f;
                EasyLoading.dismiss();
                Get.off(() => DrvLicReviewPage()).then((value) {
                  if (value != null) {
                    Get.off(() => OpenCamDrvLicPage());
                  }
                });
              }
            }),
        Positioned(
          top: getHP(context, 18),
          child: Container(
            width: getW(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Driving licence scanned",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: getHP(context, 15),
          child: Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: MyTheme.greenColor),
            child: IconButton(
                iconSize: 40,
                onPressed: () {
                  EasyLoading.show();
                  _controller.crop();
                },
                icon: Icon(
                  Icons.check,
                  color: Colors.white,
                )),
          ),
        ),
      ],
    );
  }
}
