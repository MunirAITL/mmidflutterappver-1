import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/auth/reg_page1.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/utils/ui_helper.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'more/more_view.dart';

abstract class StartBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  final scafoldKey = GlobalKey();
  Barcode result;
  QRViewController controller;

  int index = 0;

  drawLayout();

  @override
  Widget build(BuildContext context) {
    return null;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    //WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        onResumed();
        break;
      case AppLifecycleState.inactive:
        onPaused();
        break;
      case AppLifecycleState.paused:
        onInactive();
        break;
      case AppLifecycleState.detached:
        onDetached();
        break;
    }
  }

  void onResumed();
  void onPaused();
  void onInactive();
  void onDetached();

  //  **************************************  Start work on tabs

  drawTab1() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 20),
            drawCell(
                0,
                Icon(
                  Icons.account_box_outlined,
                  color: Colors.green,
                  size: 20,
                ),
                "Create a new account",
                true),
            drawCell(
                1,
                Icon(
                  Icons.add_box_outlined,
                  color: Colors.grey,
                  size: 20,
                ),
                "Scan a QR code",
                (userData.userModel == null) ? true : false),
            (userData.userModel == null)
                ? drawCell(
                    2,
                    Icon(
                      Icons.login,
                      color: Colors.green,
                      size: 20,
                    ),
                    "Login",
                    false)
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  drawCell(i, ico, title, isLine) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () {
          if (i == 0) {
            Get.to(() => RegPage1());
          } else if (i == 1) {
            //  QR Code
            index = 1;
            setState(() {});
          } else if (i == 2) {
            Get.to(() => LoginPage()).then((value) {
              setState(() {});
            });
          }
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: UIHelper().drawLine())
                : SizedBox(height: 20)
          ],
        ),
      ),
    );
  }

  drawTab2() {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            //flex: 5,
            child: Stack(
              children: [
                QRView(
                  key: qrKey,
                  onQRViewCreated: _onQRViewCreated,
                ),
                Positioned(
                    top: getHP(context, 5),
                    child: Container(
                      width: getW(context),
                      child: Center(
                        child: Text(
                          "Scan a QR code",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )),
                Positioned(
                    bottom: getHP(context, 5),
                    child: Container(
                      width: getW(context),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: Text(
                            "Hold your phone towards the QR code, so it aligns with the square above",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                            ),
                          ),
                        ),
                      ),
                    )),
              ],
            ),
          ),
          /*Expanded(
            flex: 1,
            child: Center(
              child: (result != null)
                  ? Text(
                      'Barcode Type: ${describeEnum(result.format)}   Data: ${result.code}')
                  : Text('Scan a code'),
            ),
          )*/
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller2) {
    //if (this.controller == null) {
    this.controller = controller2;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        result = scanData;
      });
    });
    //}
  }

  drawTab3() {
    //return MoreHelper().drawLayout();
    if (userData.userModel != null) {
      return Padding(
          padding: const EdgeInsets.only(top: 20),
          child: MoreHelper().drawMoreItems(() {
            CookieMgr().delCookiee();
            DBMgr.shared.delTable("User");
            userData.destroyUser();
            index = 0;
            setState(() {});
          }));
    } else {
      Future.delayed(Duration(milliseconds: 1), () {
        index = 0;
        showSnake("Login",
            "Please login first before using document scanning services");
        setState(() {});
      });
    }
  }
}
