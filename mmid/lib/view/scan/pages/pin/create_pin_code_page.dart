import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'create_pin_code_base.dart';

class CreatePinCodePage extends StatefulWidget {
  const CreatePinCodePage({Key key}) : super(key: key);
  @override
  State createState() => _CreatePinCodePageState();
}

class _CreatePinCodePageState extends CreatePinCodeBase<CreatePinCodePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    clearOtp();
    super.dispose();
  }

  void clearOtp() {
    fiveDigit = null;
    fourthDigit = null;
    thirdDigit = null;
    secondDigit = null;
    firstDigit = null;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: (!isConfirmPin) ? "Create PIN code" : "Confirm PIN code",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.greyColor,
          child: getOtpKeyboard,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getW(context),
      //height: getH(context),
//        padding: new EdgeInsets.only(bottom: 16.0),
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                height: getHP(context, 7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, top: 10, bottom: 10),
                      child: Expanded(
                        child: Txt(
                            txt: (!isConfirmPin)
                                ? "Create a PIN code to secure this smart card."
                                : "Repeat the PIN code you just entered.",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                    ),
                    isConfirmPin
                        ? TextButton(
                            onPressed: () {
                              isConfirmPin = false;
                              isPinMatched = true;
                              clearOtp();
                              setState(() {});
                            },
                            child: Text(
                              "Reset",
                              style: TextStyle(
                                color: Colors.blue.shade300,
                                fontSize: 14,
                              ),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              Center(
                child: Container(
                  color: MyTheme.greyColor,
                  width: getW(context),
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Txt(
                            txt: (!isConfirmPin)
                                ? "Enter 5 digits PIN code"
                                : "Repeat your 5 digits PIN code",
                            txtColor: Colors.black45,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.center,
                            isBold: false),
                        SizedBox(height: 20),
                        getInputField,
                      ],
                    ),
                  ),
                ),
              ),
              (!isPinMatched)
                  ? Padding(
                      padding:
                          const EdgeInsets.only(left: 20, right: 20, top: 20),
                      child: Center(
                        child: Txt(
                            txt: "Your entered PIN does not match",
                            txtColor: Colors.red,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ],
      ),
    );
  }
}
