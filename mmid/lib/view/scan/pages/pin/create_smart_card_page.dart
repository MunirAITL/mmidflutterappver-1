import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'create_pin_code_page.dart';
import 'create_smart_card_base.dart';

class CreateSmartCardPage extends StatefulWidget {
  const CreateSmartCardPage({Key key}) : super(key: key);

  @override
  State createState() => _CreateSmartCardPageState();
}

class _CreateSmartCardPageState
    extends CreateSmartCardBase<CreateSmartCardPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Create smart card",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getW(context),
              height: getHP(context, 60),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/img/smart_card_bg.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "Create a smart card",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt:
                      "To secure your Digidentity account, a smart card will be created, acting as a key to your online identity. This smart card is tied to this device, and secured with a PIN code of your choice.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                child: MMBtn(
                    txt: "Continue",
                    bgColor: MyTheme.greenColor,
                    txtColor: Colors.white,
                    width: getW(context),
                    height: getHP(context, 6),
                    radius: 0,
                    callback: () {
                      Get.to(() => CreatePinCodePage());
                    })),
          ],
        ),
      ),
    );
  }
}
