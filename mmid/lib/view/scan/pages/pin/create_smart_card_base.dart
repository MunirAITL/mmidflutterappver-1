import 'package:aitl/view/utils/mixin.dart';
import 'package:flutter/material.dart';

abstract class CreateSmartCardBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();
}
