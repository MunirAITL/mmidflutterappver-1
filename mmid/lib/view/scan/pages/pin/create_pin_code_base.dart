import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class CreatePinCodeBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  static const int PIN_LEN = 5;
  var _currentDigit;
  var firstDigit;
  var secondDigit;
  var thirdDigit;
  var fourthDigit;
  var fiveDigit;
  var lastPinCode = "";
  bool isConfirmPin = false;
  var pinCode = "";
  bool isPinMatched = true;

  drawLayout();

  // Return "OTP" input field
  get getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        otpTextField(firstDigit),
        otpTextField(secondDigit),
        otpTextField(thirdDigit),
        otpTextField(fourthDigit),
        otpTextField(fiveDigit),
      ],
    );
  }

  // Returns "Otp" keyboard
  get getOtpKeyboard {
    return new Container(
        height: getW(context) - 160,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        setCurrentDigit(1);
                      }),
                  otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        setCurrentDigit(2);
                      }),
                  otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        setCurrentDigit(4);
                      }),
                  otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        setCurrentDigit(5);
                      }),
                  otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        setCurrentDigit(7);
                      }),
                  otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        setCurrentDigit(8);
                      }),
                  otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new SizedBox(
                    width: getW(context) / 3.5,
                  ),
                  otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        setCurrentDigit(0);
                      }),
                  otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace_outlined,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            if (fiveDigit != null) {
                              fiveDigit = null;
                              if (!isPinMatched) {
                                isPinMatched = true;
                                setState(() {});
                              }
                            } else if (fourthDigit != null) {
                              fourthDigit = null;
                            } else if (thirdDigit != null) {
                              thirdDigit = null;
                            } else if (secondDigit != null) {
                              secondDigit = null;
                            } else if (firstDigit != null) {
                              firstDigit = null;
                            }
                          });
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  // Returns "Otp custom text field"
  Widget otpTextField(int digit) {
    int boxSpace = 2 * 20;
    double boxW = (getWP(context, 100) / PIN_LEN) - boxSpace;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      child: Txt(
          txt: digit != null ? "*" : "",
          txtColor: Colors.black,
          txtSize: MyTheme.txtSize + 1,
          txtAlign: TextAlign.start,
          isBold: false),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: digit != null ? MyTheme.greenColor : Colors.black,
            width: digit != null ? 2 : 1,
          ),
        ),
        //color: MyTheme.redColor,
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      elevation: 2,
      color: MyTheme.lgreyColor,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(20.0),
        child: new Container(
          height: getWP(context, 13),
          width: getW(context) / 3.5,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: new Center(
            child: Txt(
                txt: label,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + 1,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(20.0),
      child: new Container(
        height: getWP(context, 10),
        width: getW(context) / 3.5,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void setCurrentDigit(int i) {
    if (mounted) {
      setState(() {
        _currentDigit = i;
        if (firstDigit == null) {
          firstDigit = _currentDigit;
        } else if (secondDigit == null) {
          secondDigit = _currentDigit;
        } else if (thirdDigit == null) {
          thirdDigit = _currentDigit;
        } else if (fourthDigit == null) {
          fourthDigit = _currentDigit;
        } else if (fiveDigit == null) {
          fiveDigit = _currentDigit;
          final pinCode2 = firstDigit.toString() +
              secondDigit.toString() +
              thirdDigit.toString() +
              fourthDigit.toString() +
              fiveDigit.toString();
          if (!isConfirmPin) {
            firstDigit = null;
            secondDigit = null;
            thirdDigit = null;
            fourthDigit = null;
            fiveDigit = null;
            pinCode = pinCode2;
            isConfirmPin = true;
            setState(() {});
          } else {
            if (pinCode2 != pinCode) {
              isPinMatched = false;
            } else {
              isPinMatched = true;
            }

            setState(() {});
          }
        }
      });
    }
  }
}
