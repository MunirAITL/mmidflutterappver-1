import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/scan/pages/pin/create_smart_card_page.dart';
import 'package:aitl/view/scan/pages/smart_card/creating_smart_card_page.dart';
import 'package:aitl/view/scan/selfie/selfie_page.dart';
import 'package:aitl/view/utils/ui_helper.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../scan_id_doc_page.dart';

class MoreHelper {
  drawMoreItems(Function callbackSignout) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            drawMoreCell(
                0,
                Icon(
                  Icons.document_scanner,
                  color: Colors.green,
                  size: 20,
                ),
                "Scan Documents",
                true,
                callbackSignout),
            drawMoreCell(
                1,
                Icon(
                  Icons.camera,
                  color: Colors.green,
                  size: 20,
                ),
                "Take a selfie",
                true,
                callbackSignout),
            drawMoreCell(
                2,
                Icon(
                  Icons.account_box,
                  color: Colors.green,
                  size: 20,
                ),
                "Pin code",
                true,
                callbackSignout),
            drawMoreCell(
                3,
                Icon(
                  Icons.smart_button,
                  color: Colors.green,
                  size: 20,
                ),
                "Smart card page",
                true,
                callbackSignout),
            drawMoreCell(
                4,
                Icon(
                  Icons.domain_verification,
                  color: Colors.green,
                  size: 20,
                ),
                "E-ID Verification",
                true,
                callbackSignout),
            drawMoreCell(
                5,
                Icon(
                  Icons.logout,
                  color: Colors.green,
                  size: 20,
                ),
                "Logout",
                false,
                callbackSignout),
          ],
        ),
      ),
    );
  }

  drawMoreCell(i, ico, title, isLine, Function callbackSignout) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () {
          if (i == 0) {
            Get.to(() => ScanIDDocPage());
          } else if (i == 1) {
            Get.to(() => SelfiePage(isMorePage: true));
          } else if (i == 2) {
            Get.to(() => CreateSmartCardPage());
          } else if (i == 3) {
            Get.to(() => CreatingSmartCardPage());
          } else if (i == 4) {
            Get.to(
              () => WebScreen(
                  ewebType: eWebType.WEB,
                  url: Server.EID_URL,
                  title: "E-ID Verification"),
              fullscreenDialog: true,
            );
          } else if (i == 5) {
            callbackSignout();
          }
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: UIHelper().drawLine())
                : SizedBox(
                    height: 20,
                  )
          ],
        ),
      ),
    );
  }
}
