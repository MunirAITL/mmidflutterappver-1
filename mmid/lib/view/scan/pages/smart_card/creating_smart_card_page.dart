import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/scan/pages/smart_card/completed_smart_card_page.dart';
import 'package:aitl/view/scan/pages/smart_card/creating_smart_card_base.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreatingSmartCardPage extends StatefulWidget {
  const CreatingSmartCardPage({Key key}) : super(key: key);

  @override
  State createState() => _CreatingSmartCardPageState();
}

class _CreatingSmartCardPageState
    extends CreatingSmartCardBase<CreatingSmartCardPage> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 2), () {
      isLoading = false;
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Authenticator",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: MyTheme.bgColor,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Container(
                width: getWP(context, 50),
                height: getHP(context, 5),
                child: Image.asset("assets/images/logo/logo.png")),
          ),
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: (isLoading)
                      ? "Creating your smart card..."
                      : "Great, your smart card is completed and ready to use on each time when you log in with your Digidentity account.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Material(
                elevation: 2,
                color: MyTheme.greyColor,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 10, bottom: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: (isLoading) ? 5 : 0),
                        child: (isLoading)
                            ? SizedBox(
                                child: CircularProgressIndicator(
                                  color: Colors.black54,
                                  strokeWidth: 2.0,
                                ),
                                height: 15,
                                width: 15,
                              )
                            : Icon(
                                Icons.manage_accounts,
                                color: Colors.black,
                              ),
                      ),
                      SizedBox(width: 20),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Txt(
                                txt: "smhafiz@aitl.net",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            SizedBox(height: 5),
                            Txt(
                                txt: "Personal smart card, Level 0",
                                txtColor: Colors.black45,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                child: MMBtn(
                    txt: "Continue",
                    bgColor: MyTheme.greenColor,
                    txtColor: Colors.white,
                    width: getW(context),
                    height: getHP(context, 6),
                    radius: 0,
                    callback: () {
                      Get.off(() => CompletedSmartCardPage());
                    })),
          ],
        ),
      ),
    );
  }
}
