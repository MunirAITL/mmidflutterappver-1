import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/scan/pages/smart_card/completed_smart_card_base.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CompletedSmartCardPage extends StatefulWidget {
  const CompletedSmartCardPage({Key key}) : super(key: key);
  @override
  State createState() => _CompletedSmartCardPageState();
}

class _CompletedSmartCardPageState
    extends CompletedSmartCardBase<CompletedSmartCardPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Smart card completed",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: MyTheme.bgColor,
          child: Padding(
            padding:
                const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Txt(
                    txt:
                        "Return to the website for further instructions to complete your registration.",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
                SizedBox(height: 20),
                MMBtn(
                    txt: "Done",
                    bgColor: MyTheme.greenColor,
                    txtColor: Colors.white,
                    width: getW(context),
                    height: getHP(context, 6),
                    radius: 0,
                    callback: () {
                      Get.back();
                    }),
              ],
            ),
          ), /*Padding(
            padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
            child: Column(
              children: [
                Txt(
                    txt:
                        "Return to the website for further instructions to complete your registration.",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
                SizedBox(height: 10),
                MMBtn(
                    txt: "Done",
                    bgColor: MyTheme.greenColor,
                    txtColor: Colors.white,
                    width: getW(context),
                    height: getHP(context, 6),
                    radius: 0,
                    callback: () {}),
              ],
            ),*/
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Center(
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.manage_accounts,
                color: MyTheme.greenColor,
                size: 60,
              ),
              SizedBox(height: 5),
              Txt(
                  txt: "Smart card successfully created",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
