import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'start_base.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class StartPage extends StatefulWidget {
  @override
  State createState() => StartPageState();
}

class StartPageState extends StartBase<StartPage> with Mixin {
  final List<Widget> listTabbar = [];

  final listTab = [
    {
      "index": 0,
      "title": "Authenticators",
      "icon": Icons.security,
    },
    {"index": 1, "title": "Scan QR", "icon": Icons.qr_code},
    {"index": 2, "title": "More", "icon": Icons.more_horiz},
  ];

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  /*@override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }*/

  //_selectTab(int index) {}

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      controller?.dispose();
      result = null;
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {}

  @override
  Widget build(BuildContext context) {
    final w = (getW(context) / listTab.length);
    final h = getHP(context, 10);
    return SafeArea(
      child: Scaffold(
        key: scafoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Authenticators",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0,
          color: MyTheme.greyColor,
          child: Container(
            height: h,
            //color: Colors.white,
            alignment: Alignment.bottomCenter,
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    //if (controller != null) controller.pauseCamera();
                    controller?.dispose();
                    index = 0;
                    setState(() {});
                  },
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Material(
                      elevation: 0,
                      child: Container(
                        width: w + 20,
                        height: h - 15,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey, width: 0.1)),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Icon(
                                  Icons.security,
                                  size: 25,
                                  color:
                                      (index == 0) ? Colors.red : Colors.black,
                                ),
                              ),
                              Txt(
                                txt: "Authenticators",
                                txtColor:
                                    (index == 0) ? Colors.red : Colors.black,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.center,
                                isBold: false,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    //if (controller != null) controller.resumeCamera();
                    index = 1;
                    setState(() {});
                  },
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Material(
                      elevation: 20,
                      child: Container(
                        width: w - 40,
                        height: h,
                        decoration: BoxDecoration(
                            color: (index == 1) ? Colors.green : Colors.white,
                            border: Border.all(color: Colors.grey, width: 0.1)),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Icon(
                                  Icons.qr_code,
                                  size: 30,
                                  color: (index == 1)
                                      ? Colors.white
                                      : Colors.black,
                                ),
                              ),
                              Txt(
                                txt: "Scan QR",
                                txtColor:
                                    (index == 1) ? Colors.white : Colors.black,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.center,
                                isBold: false,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    //if (controller != null) controller.pauseCamera();
                    controller?.dispose();
                    index = 2;
                    setState(() {});
                  },
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Material(
                      elevation: 0,
                      child: Container(
                        width: w + 20,
                        height: h - 15,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey, width: 0.1)),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Icon(
                                  Icons.more_horiz,
                                  size: 30,
                                  color:
                                      (index == 2) ? Colors.red : Colors.black,
                                ),
                              ),
                              Txt(
                                txt: "More",
                                txtColor:
                                    (index == 2) ? Colors.red : Colors.black,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.center,
                                isBold: false,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    switch (index) {
      case 0:
        return drawTab1();
        break;
      case 1:
        return drawTab2();
        break;
      case 2:
        return drawTab3();
        break;
      default:
        return Container();
    }
  }
}
