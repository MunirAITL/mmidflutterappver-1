import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/scan/driving_lic/drvlic_page1.dart';
import 'package:aitl/view/scan/eu/eu_open_cam_page.dart';
import 'package:aitl/view/scan/eu/eu_page1.dart';
import 'package:aitl/view/scan/passport/pp_open_cam_page.dart';
import 'package:aitl/view/scan/passport/pp_page1.dart';
import 'package:aitl/view/scan/selfie/selfie_open_cam_page.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/utils/ui_helper.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class ScanIDDocBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();

  drawRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "What identity document do you want to scan?",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            SizedBox(height: 10),
            drawCell(
                0,
                Icon(
                  Icons.drive_eta_outlined,
                  color: Colors.green,
                  size: 20,
                ),
                "Driving licence",
                true),
            drawCell(
                1,
                Icon(
                  Icons.account_box,
                  color: Colors.green,
                  size: 20,
                ),
                "Passport",
                false),
            /*drawCell(
                2,
                Icon(
                  Icons.account_box,
                  color: Colors.green,
                  size: 20,
                ),
                "EU card",
                false),*/
          ],
        ),
      ),
    );
  }

  drawCell(i, ico, title, isLine) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () {
          if (i == 0) {
            //  Driving Licence
            Get.to(() => DrvLicPage1());
          } else if (i == 1) {
            //  Passport
            Get.to(() => PPPage1());
          } else if (i == 2) {
            //  EU card
            Get.to(() => EUPage1());
          }
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: UIHelper().drawLine())
                : SizedBox(
                    height: 20,
                  )
          ],
        ),
      ),
    );
  }
}
