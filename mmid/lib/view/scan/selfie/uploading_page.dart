import 'dart:convert';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:typed_data';

import 'package:aitl/config/server/APIMediaCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/media_upload/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/data/model/media_upload/PostDocVerifyAPIModel.dart';
import 'package:aitl/data/model/media_upload/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/scan/pages/scan_id_doc_page.dart';
import 'package:aitl/view/scan/passport/pp_open_cam_page.dart';
import 'package:aitl/view/scan/selfie/selfie_open_cam_page.dart';
import 'package:aitl/view/scan/selfie/uploading_base.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mime_type/mime_type.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class UploadingPage extends StatefulWidget {
  const UploadingPage({Key key}) : super(key: key);

  @override
  State createState() => _UploadingPageState();
}

class _UploadingPageState extends UploadingBase<UploadingPage> {
  bool isUploadingDone = true;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //progController.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  initPage() async {
    try {
      String mimeType = mime(appData.file_selfie.path);
      var fileName = (appData.file_selfie.path.split('/').last);
      //String mimee = mimeType.split('/')[0];
      String type = mimeType.split('/')[1];
      List<int> fileInByte = appData.file_selfie.readAsBytesSync();
      final imageData = base64Encode(fileInByte);
      print(imageData);
      await APIViewModel().req<PostSelfieByBase64DataAPIModel>(
          context: context,
          url: APIMediaCfg.SELFIE_POST_URL,
          reqType: ReqType.Post,
          param: {
            "UserCompanyId": userData.userModel.userCompanyID,
            "ImageType": type,
            "FileName": fileName,
            "ContentData": imageData,
          },
          callback: (model) async {
            if (model != null && mounted) {
              if (model.success) {
                showAlert(
                    msg: 'Selfie has been uploaded successfully.',
                    isToast: true);
                Future.delayed(Duration(seconds: 1), () {
                  Get.off(() => ScanIDDocPage());
                });
                //
                /*progController.progress.value = 0;
                //Uint8List bytes =
                // base64Decode(base64Encode(widget.file.readAsBytesSync()));
                await APIViewModel().req<PostDocByBase64BitAPIModel>(
                    context: context,
                    url: APIMediaCfg.DOC_POST_URL,
                    reqType: ReqType.Post,
                    headers: {
                      "Accept": "application/json",
                      "Content-Type": "application/x-www-form-urlencoded",
                    },
                    param: {
                      "UserCompanyId": userData.userModel.userCompanyID,
                      "ImageType": type,
                      "FileName": fileName,
                      "ContentData": imageData,
                      "DocumentType": "0",
                      "RequestId": model.responseData.responsedata.requestid,
                    },
                    callback: (model2) async {
                      if (model2 != null && mounted) {
                        if (model2.success) {
                          //
                          progController.progress.value = 0;
                          //Uint8List bytes =
                          // base64Decode(base64Encode(widget.file.readAsBytesSync()));
                          await APIViewModel().req<PostDocVerifyAPIModel>(
                              context: context,
                              url: APIMediaCfg.VERIFY_POST_URL,
                              reqType: ReqType.Post,
                              param: {
                                "Requestid":
                                    model.responseData.responsedata.requestid,
                              },
                              callback: (model3) {
                                if (model3 != null && mounted) {
                                  if (model3.success) {
                                    showAlert(
                                        msg:
                                            'Selfie has been uploaded successfully.',
                                        isToast: true);
                                    Future.delayed(Duration(seconds: 1), () {
                                      Get.back();
                                    });
                                  } else {
                                    isUploadingDone = false;
                                    setState(() {});
                                  }
                                } else {
                                  isUploadingDone = false;
                                  setState(() {});
                                }
                              });
                        } else {
                          isUploadingDone = false;
                          setState(() {});
                        }
                      } else {
                        isUploadingDone = false;
                        setState(() {});
                      }
                    });*/
              } else {
                isUploadingDone = false;
                setState(() {});
              }
            } else {
              isUploadingDone = false;
              setState(() {});
            }
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Take your selfie",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          /*actions: [
            IconButton(
                onPressed: () {
                  Get.off(() => OpenCamPPPage());
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],*/
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Center(
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.cloud_upload,
                color: MyTheme.greenColor,
                size: 60,
              ),
              SizedBox(height: 5),
              Txt(
                  txt: "Uploading the documents",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Obx(() => Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: LinearProgressIndicator(
                      backgroundColor: Colors.grey,
                      valueColor:
                          AlwaysStoppedAnimation<Color>(MyTheme.greenColor),
                      value: progController.progress.value,
                    ),
                  )),
              (!isUploadingDone)
                  ? Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 20, right: 20),
                      child: MMBtn(
                          txt: "Re-take",
                          bgColor: MyTheme.greyColor,
                          txtColor: Colors.black,
                          width: getWP(context, 40),
                          height: getHP(context, 6),
                          radius: 0,
                          callback: () {
                            Get.off(() => OpenCamSelfiePage());
                          }))
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
