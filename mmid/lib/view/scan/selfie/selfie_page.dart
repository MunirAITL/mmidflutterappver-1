import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/scan/selfie/selfie_base.dart';
import 'package:aitl/view/scan/selfie/selfie_open_cam_page.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SelfiePage extends StatefulWidget {
  final bool isMorePage;
  const SelfiePage({Key key, this.isMorePage}) : super(key: key);
  @override
  State createState() => _SelfiePageState();
}

class _SelfiePageState extends SelfieBase<SelfiePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Prove your identity",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            (widget.isMorePage)
                ? IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      Icons.close,
                      color: MyTheme.greenColor,
                      size: 20,
                    ))
                : SizedBox()
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    //print(userData.userModel.userCompanyID);
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getW(context),
              height: getHP(context, 55),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/img/selfie_bg.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "Take a selfie",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt:
                      "To prove that it is really you on the identity document, you need to take a photo of yourself, a selfie. Make sure your face is clearly visible and you're the only person in the photo.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                child: MMBtn(
                    txt: "Continue",
                    bgColor: MyTheme.greenColor,
                    txtColor: Colors.white,
                    width: getW(context),
                    height: getHP(context, 6),
                    radius: 0,
                    callback: () {
                      Get.off(() => OpenCamSelfiePage());
                    })),
          ],
        ),
      ),
    );
  }
}
