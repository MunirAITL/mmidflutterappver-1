import 'package:aitl/view/utils/fun.dart';
import 'package:aitl/view/utils/parser.dart';
import 'package:aitl/view/utils/scan_mixin.dart';
import 'package:aitl/view/utils/ui_mixin.dart';
import 'package:flutter/material.dart';

abstract class BaseCard<T extends StatefulWidget> extends State<T>
    with ScanMixin, Fun, UIMixin, ParserMixin {}
