import 'dart:io';
import 'package:aitl/config/cfg/Define.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/view/scan/eu/euid_mixin.dart';
import 'package:aitl/view/utils/fun.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/utils/scan_mixin.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/anim/ImageScannerAnimation.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view_model/rx/gender_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class EUReviewBase<T extends StatefulWidget> extends State<T>
    with
        Mixin,
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin,
        ScanMixin,
        EUIDMixin {
  bool isFrontAnim = false;
  bool isBackAnim = false;

  AnimationController animationController;

  //  front
  final cardNo = TextEditingController();
  final mumerCan = TextEditingController();
  final surName = TextEditingController();
  final givenName = TextEditingController();
  final personalNo = TextEditingController();
  final identityCardNo = TextEditingController();
  DropListModel ddMaritalNationalities;
  OptionItem optNationalities;
  GenderController cGender = Get.put(GenderController());
  var dob = "";
  var dtExpiry = "";
  bool isAnim = true;
  bool isData1 = true;

  //  back
  final familyName = TextEditingController();
  final placeBirth = TextEditingController();
  final authority = TextEditingController();
  final height = TextEditingController();
  final personName = TextEditingController();
  var dtIssue = "";

  drawLayout();

  drawPicBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Stack(
            children: [
              Container(
                width: getWP(context, 43),
                height: getHP(context, 15),
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    border: Border.all(color: MyTheme.greenColor, width: 1)),
                child: Image.file(
                  appData.file_eu_front,
                  fit: BoxFit.fill,
                ),
              ),
              (isFrontAnim)
                  ? ImageScannerAnimation(
                      !isFrontAnim,
                      getWP(context, 45),
                      animation: animationController,
                    )
                  : SizedBox()
            ],
          ),
          SizedBox(width: 5),
          Stack(
            children: [
              Container(
                width: getWP(context, 43),
                height: getHP(context, 15),
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    border: Border.all(color: MyTheme.greenColor, width: 1)),
                child: Image.file(
                  appData.file_eu_back,
                  fit: BoxFit.fill,
                ),
              ),
              (isBackAnim)
                  ? ImageScannerAnimation(
                      !isBackAnim,
                      getWP(context, 45),
                      animation: animationController,
                    )
                  : SizedBox()
            ],
          ),
        ],
      ),
    );
  }

  drawIssues(List<String> list) {
    return Container(
      color: MyTheme.l3BlueColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: list.length,
            itemBuilder: (context, index) {
              return Txt(
                  txt: list[index],
                  txtColor: Colors.blue,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false);
            }),
      ),
      //return Txt(txt: list[i], txtColor: Colors.blue, txtSize: MyTheme.txtSize, txtAlign: TextAlign.start, isBold: false)}),
    );
  }

  void animateScanAnimation(bool reverse) {
    if (reverse) {
      animationController.reverse(from: 1.0);
    } else {
      animationController.forward(from: 0.0);
    }
  }

  //  start google vision api
  doFrontOCR(Function callback) {
    doGoogleVisionOCR(appData.file_eu_front).then((body) {
      if (body != null) {
        log(body);
        parseFrontSide(body);
        callback();
      } else {
        showAlert(msg: 'failed to parse EU Card - front');
        callback();
      }
    });
  }

  parseFrontSide(String gbody) {
    try {
      //  Try with google vision
      var gbody2 = gbody.replaceAll(".\\n", ".");
      final gbodyArr = gbody2.split("\\n");

      //var gbody2 = gbody.replaceAll(".\n", ".");
      //final gbodyArr = gbody2.split("\n");

      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "").toLowerCase();
        print(v);

        if (v.toLowerCase().startsWith("latvijas r")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          String mCardNo = getCardNo(v2);
          cardNo.text = mCardNo;
        }
        if (v.toLowerCase().startsWith("mumer can")) {
          String str = v.toLowerCase().replaceAll("mumer can", "").trim();
          if (str.length > 0) {
            mumerCan.text = str;
          } else {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            try {
              if (int.parse(v2) > 0) mumerCan.text = v2;
            } catch (e) {}
          }
        }
        if (v.toLowerCase().startsWith("signature d")) {
          try {
            final strArr = v.split(" ");
            if (int.parse(strArr[strArr.length - 1]) > 0) {
              mumerCan.text = strArr[strArr.length - 1];
            }
          } catch (e) {}
          if (mumerCan.text.isEmpty) {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            try {
              if (int.parse(v2) > 0) {
                mumerCan.text = v2;
              }
            } catch (e) {}
          }
        }
        if (v.toLowerCase().startsWith("nazwisko /") ||
            v.toLowerCase().startsWith("Uzvards/") ||
            v.toLowerCase().contains("surname")) {
          try {
            final txt2 = gbodyArr[i + 1];
            final v2 = txt2.replaceAll("\n", "");
            surName.text = v2;
          } catch (e) {}
        } else if (v.toLowerCase().startsWith("Vardsl") ||
            v.toLowerCase().startsWith("mona /") ||
            v.toLowerCase().contains("given n")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          givenName.text = v2;
        } else if (v.toLowerCase().contains("nationality")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          if (v2.contains("-")) {
            final strArr = v2.split(" ");
            if (strArr.length == 1) {
              personalNo.text = strArr[0];
            } else if (strArr.length > 1) {
              personalNo.text = strArr[0];
              optNationalities.title = strArr[1];
            }
          }

          if (optNationalities.title == "Nationality") {
            bool isString = Fun.hasStringOnly(v2);
            if (isString) {
              optNationalities.title = v2;
            }
          }
          if (personalNo.text.isEmpty) {
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
            if (v2.contains("-")) {
              personalNo.text = v2;
            }
          }
        }
        if (v.toLowerCase().contains("date of birth")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          try {
            final strArr = v2.split(".");
            if (strArr.length > 2) {
              int pos = int.parse(strArr[1]);
              String mm = Define.listMMM[pos - 1];
              dob = strArr[0] + "-" + mm + "-" + strArr[2];
            }
          } catch (e) {}
          if (dob == "") {
            String txt2 = gbodyArr[i + 2];
            String v2 = txt2.replaceAll("\n", "");
            try {
              final strArr = v2.split(".");
              if (strArr.length > 2) {
                int pos = int.parse(strArr[1]);
                String mm = Define.listMMM[pos - 1];
                dob = strArr[0] + "-" + mm + "-" + strArr[2];
              }
            } catch (e) {}
          }
        }
        if ((v.toLowerCase().contains("termin") ||
                v.toLowerCase().contains("expiry")) &&
            v.toLowerCase().contains("date")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          try {
            final strArr = v2.split(".");
            if (strArr.length > 2) {
              int pos = int.parse(strArr[1]);
              String mm = Define.listMMM[pos - 1];
              dtExpiry = strArr[0] + "-" + mm + "-" + strArr[2];
            }
          } catch (e) {}
        }
        if (v.toLowerCase().contains("date of expiry")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "");
          try {
            final strArr = v2.split(".");
            if (strArr.length > 2) {
              int pos = int.parse(strArr[1]);
              String mm = Define.listMMM[pos - 1];
              dtExpiry = strArr[0] + "-" + mm + "-" + strArr[2];
            }
          } catch (e) {}
        }
        if (v.toLowerCase().endsWith("sex")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.length == 1) {
            if (v2 == 'm') cGender.gender = genderEnum.male.obs;
            if (v2 == 'f') cGender.gender = genderEnum.female.obs;
          }
          txt2 = gbodyArr[i + 2];
          v2 = txt2.replaceAll("\n", "");
          if (v2.length > 0) {
            identityCardNo.text = v2;
          }
        }
      }
    } catch (e) {}
  }

  doBackOCR(Function callback) {
    doGoogleVisionOCR(appData.file_eu_back).then((body) {
      isBackAnim = true;
      setState(() {});
      if (body != null) {
        log(body);
        parseBackSide(body);
        callback();
      } else {
        showAlert(msg: 'failed to parse EU Card - back');
        callback();
      }
    });
  }

  parseBackSide(gbody) {
    try {
      //var gbody2 = gbody.replaceAll(".\\n", ".");
      //final gbodyArr = gbody2.split("\\n");

      var gbody2 = gbody.replaceAll(".\n", ".");
      final gbodyArr = gbody2.split("\n");

      for (int i = 0; i < gbodyArr.length; i++) {
        final txt = gbodyArr[i];
        final v = txt.replaceAll("\n", "").toLowerCase();
        print(v);

        //  height
        if (v.startsWith("augums ") ||
            v.contains("height") ||
            v.endsWith("taille")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.startsWith("dzimums	") || v2.contains("sex")) {
            //  height
            String txt2 = gbodyArr[i + 2];
            String v2 = txt2.replaceAll("\n", "").toLowerCase();
            try {
              if (int.parse(v2) > 0) {
                height.text = v2;
              }
            } catch (e) {}
            //  sex
            //
            txt2 = gbodyArr[i + 3];
            v2 = txt2.replaceAll("\n", "").toLowerCase();
            if (v2.length < 4) {
              if (v2.contains('m')) cGender.gender = genderEnum.male.obs;
              if (v2.contains('f')) cGender.gender = genderEnum.female.obs;
            }
          } else {
            try {
              if (int.parse(v2) > 0) {
                height.text = v2;
              }
            } catch (e) {}
          }
        }
        //  sex
        if (v.startsWith("dzimums	") || v.contains("sex")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.length < 4) {
            if (v2.contains('m')) cGender.gender = genderEnum.male.obs;
            if (v2.contains('f')) cGender.gender = genderEnum.female.obs;
          }
        }
        //  date of issue 1st try
        if (v.startsWith("data wydania")) {
          final txt2 = gbodyArr[i + 2];
          final v2 = txt2.replaceAll("\n", "");
          final strArr = v2.split(".");
          if (strArr.length == 3) {
            int pos = int.parse(strArr[1]);
            String mm = Define.listMMM[pos - 1];
            dtIssue = strArr[0] + "-" + mm + "-" + strArr[2];
          }
        }
        //  date of issue 2nd try
        if (dtIssue == '' && v.startsWith("izdošanas	") ||
            (v.contains("date") && v.contains("issue"))) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          final strArr = v2.split(".");
          if (strArr.length > 2) {
            int pos = int.parse(strArr[1]);
            String mm = Define.listMMM[pos - 1];
            dtIssue = strArr[0] + "-" + mm + "-" + strArr[2];
          }
        }
        //  authority
        if ((v.startsWith("izdevėjiestade") || v.startsWith("organ")) ||
            v.contains("authority")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          authority.text = v2;
        }
        //  historical person name
        if (v.contains("historic") && v.contains("person")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          personName.text = v2;
        }
        //  Personal number
        if (v.startsWith("numer pe") || v.contains("personal num")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          personalNo.text = v2;
        }
        //  identity card number try 1
        if (v.startsWith("numer dowoou")) {
          final txt2 = gbodyArr[i + 4];
          final v2 = txt2.replaceAll("\n", "");
          identityCardNo.text = v2;
        }
        //  identity card number try 1
        if (identityCardNo.text.isEmpty && v.startsWith("identity card")) {
          final txt2 = gbodyArr[i + 3];
          final v2 = txt2.replaceAll("\n", "");
          identityCardNo.text = v2;
        }
        //  place of birth
        if (v.startsWith("mieisce") ||
            (v.contains("place") && v.contains("birth"))) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          placeBirth.text = v2;
        }
        //  family name
        if (v.startsWith("nazwsko") || v.contains("family n")) {
          final txt2 = gbodyArr[i + 1];
          final v2 = txt2.replaceAll("\n", "");
          familyName.text = v2;
        }
        //  given name
        if (v.startsWith("imona") || v.contains("given n")) {
          String txt2 = gbodyArr[i + 1];
          String v2 = txt2.replaceAll("\n", "").toLowerCase();
          if (v2.startsWith("data wydan")) {
            txt2 = gbodyArr[i + 2];
            v2 = txt2.replaceAll("\n", "");
          }
          if (v2.startsWith("date ") && v2.contains("issue")) {
            txt2 = gbodyArr[i + 3];
            v2 = txt2.replaceAll("\n", "");
          }
          givenName.text = v2;
        }

        /*String v2 = genericParser(
          i,
          gbodyArr,
          v,
          [
            {
              'str': 'height',
              'startWith': false,
              'contains': true,
              'endWith': false
            },
          ],
          [
            {
              'str': 'sex',
              'startWith': false,
              'contains': true,
              'endWith': false
            },
          ],
          null,
        );
        print(v2);*/
      }
    } catch (e) {}
  }
}
