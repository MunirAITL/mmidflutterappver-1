import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/view/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/scan/pages/scan_id_doc_page.dart';
import 'package:aitl/view/scan/pages/start_page.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'eu_open_cam_page.dart';
import 'eu_review_base.dart';

class EUReviewPage extends StatefulWidget {
  const EUReviewPage({Key key}) : super(key: key);
  @override
  State createState() => _EUReviewPageState();
}

class _EUReviewPageState extends EUReviewBase<EUReviewPage> {
  final list = [
    "Check the following:",
    "* Image is sharp",
    "* There are no reflections",
    "* All personal details are visible"
  ];

  @override
  void initState() {
    animationController = new AnimationController(
        duration: new Duration(seconds: 1), vsync: this);
    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animateScanAnimation(true);
      } else if (status == AnimationStatus.dismissed) {
        animateScanAnimation(false);
      }
    });
    animateScanAnimation(false);
    setState(() {
      isFrontAnim = false;
      isBackAnim = false;
    });

    super.initState();
  }

  @override
  void dispose() {
    if (animationController != null) animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Prove your identity",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.off(() => ScanIDDocPage());
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Container(
              color: MyTheme.greyColor,
              child: Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawPicBox(),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          UIHelper().drawLine(h: 1),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Review photos of EU card",
                              txtColor: Colors.black87,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          drawIssues(list),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 20, left: 20, right: 20, bottom: 10),
              child: (!isFrontAnim && !isBackAnim)
                  ? MMBtn(
                      txt: "Confirm",
                      bgColor: MyTheme.greenColor,
                      txtColor: Colors.white,
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 0,
                      callback: () {
                        isFrontAnim = true;
                        isBackAnim = false;
                        setState(() {});
                        doFrontOCR(() {
                          isFrontAnim = false;
                          isBackAnim = true;
                          setState(() {});
                          doBackOCR(() {
                            isBackAnim = false;
                            setState(() {});
                          });
                        });
                      })
                  : SizedBox(),
            ),
            (!isFrontAnim && !isBackAnim)
                ? Center(
                    child: TextButton(
                      onPressed: () {
                        appData.file_eu_front = null;
                        appData.file_eu_back = null;
                        Get.off(() => OpenCamEUPage());
                      },
                      child: Text(
                        "Rescan EU card",
                        style: TextStyle(
                          color: Colors.black38,
                          fontSize: 17,
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
