import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/country_picker/ola_like_country_picker.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/InputBoxHT.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/TCView.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'reg_page2.dart';
import 'package:get/get.dart';
import 'auth_base.dart';

class RegPage1 extends StatefulWidget {
  @override
  State createState() => _RegPage1State();
}

enum genderEnum { male, female }

class _RegPage1State extends BaseAuth<RegPage1> with APIStateListener {
  //  reg1
  final fname = TextEditingController();
  final lname = TextEditingController();
  final email = TextEditingController();
  final pwd = TextEditingController();

  CountryPicker c;
  Country country = Country.fromJson(countryCodes[94]);
  var countryFlag = "assets/images/flags/" + AppDefine.COUNTRY_FLAG + ".png";
  var countryDialCode = AppDefine.COUNTRY_DIALCODE;
  var countryName = AppDefine.COUNTRY_NAME;
  final mobile = TextEditingController();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel, false);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel, false);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    pwd.dispose();
    fname.dispose();
    lname.dispose();
    mobile.dispose();
    c = null;
    country = null;
    countryFlag = null;
    countryDialCode = null;
    countryName = null;
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      c = CountryPicker(onCountrySelected: (Country country) {
        log(country);
        setState(() {
          this.country = country;
          countryName = country.name;
          countryFlag = country.flagUri;
          countryDialCode = country.dialCode;
        });
      });
    } catch (e) {}
  }

  bool validate() {
    if (!UserProfileVal().isFNameOK(fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(lname)) {
      return false;
    } else if (!UserProfileVal().isEmailOK(email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(mobile)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Create an account",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getW(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 50),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: [
                    Txt(
                        txt: "Let's Get Started",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + 1,
                        txtAlign: TextAlign.center,
                        isBold: false),
                    SizedBox(height: 10),
                    Txt(
                        txt: "Create an account to get started",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .5,
                        txtAlign: TextAlign.center,
                        isBold: false),
                    SizedBox(height: 20),
                    InputBox(
                      ctrl: fname,
                      lableTxt: "First name",
                      kbType: TextInputType.name,
                      len: 20,
                      ecap: eCap.Word,
                    ),
                    InputBox(
                      ctrl: lname,
                      lableTxt: "Last name",
                      kbType: TextInputType.name,
                      len: 20,
                      ecap: eCap.Word,
                    ),
                    InputBox(
                      ctrl: email,
                      lableTxt: "Email",
                      kbType: TextInputType.emailAddress,
                      len: 50,
                    ),
                    GestureDetector(
                      onTap: () {
                        c.launch(context);
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                //width: getWP(context, 30),
                                color: Colors.grey.withOpacity(.45),
                                child: Row(
                                  children: [
                                    SizedBox(width: 5),
                                    Image.asset(
                                      countryFlag,
                                      width: 50,
                                      height: 50,
                                    ),
                                    //SizedBox(width: 3),
                                    Expanded(
                                      child: Text(
                                        "+" +
                                            countryDialCode.replaceAll("+", ""),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.black,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              //flex: 2,
                              child: InputBox(
                                ctrl: mobile,
                                lableTxt: "Mobile number",
                                kbType: TextInputType.phone,
                                len: 15,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    InputBox(
                      ctrl: pwd,
                      lableTxt: "Password",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true,
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 20),
                      child: Center(child: TCView(enum_tc: eTC.REG_TC)),
                    ),
                    MMBtn(
                      txt: "Continue",
                      bgColor: MyTheme.greenColor,
                      txtColor: Colors.white,
                      width: getW(context),
                      height: getHP(context, MyTheme.btnHpa),
                      radius: 0,
                      callback: () async {
                        if (validate()) {
                          Get.to(() => RegPage2(
                                fname: fname.text.trim(),
                                lname: lname.text.trim(),
                                email: email.text.trim(),
                                pwd: pwd.text.trim(),
                                mobile: mobile.text.trim(),
                                countryDialCode: countryDialCode,
                              ));
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Txt(
              txt: "Or continue with",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa + 2),
                    width: getW(context),
                    icon: "ic_fb",
                    txt: "Continue with Facebook",
                    callback: () {
                      loginWithFB(this.runtimeType);
                    },
                  ),
                  SizedBox(height: 20),
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa + 2),
                    width: getW(context),
                    icon: "ic_google",
                    txt: "Continue with Google",
                    callback: () {
                      loginWithGoogle(this.runtimeType);
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
