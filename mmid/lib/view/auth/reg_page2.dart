import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/DatePickerView.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'auth_base.dart';
import 'otp/sms_page3.dart';

class RegPage2 extends StatefulWidget {
  final mobile;
  final fname;
  final lname;
  final email;
  final pwd;
  final countryDialCode;

  const RegPage2({
    Key key,
    @required this.mobile,
    @required this.fname,
    @required this.lname,
    @required this.email,
    @required this.pwd,
    @required this.countryDialCode,
  }) : super(key: key);
  @override
  State createState() => _RegPage2State();
}

enum genderEnum { male, female }

class _RegPage2State extends BaseAuth<RegPage2> with APIStateListener {
  //  reg2
  String dob = "";
  String dobDD = "";
  String dobMM = "";
  String dobYY = "";

  genderEnum _gender = genderEnum.male;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.login && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            checkLoginRes(model as LoginAPIModel, true);
          } else {
            showAlert(
                msg: "You have entered invalid credentials", isToast: true);
          }
        }
      }
      if (apiState.type == APIType.reg && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            if (Server.isTest) {
              wsLogin();
            } else {
              if (model.responseData.user.isMobileNumberVerified) {
                wsLogin();
              } else {
                APIViewModel().req<MobileUserOtpPostAPIModel>(
                    context: context,
                    url: APIAuthCfg.LOGIN_MOBILE_OTP_POST_URL,
                    reqType: ReqType.Post,
                    param: {
                      "MobileNumber": userData.userModel.mobileNumber,
                      "OTPCode": "",
                      "Status": 101,
                      "UserId": (userData.userModel != null)
                          ? userData.userModel.id
                          : 0,
                    },
                    callback: (model2) {
                      if (model2 != null && mounted) {
                        if (model2.success) {
                          Get.to(
                            () => Sms3Page(
                              mobileUserOTPModel: model2.responseData.userOTP,
                              mobile: widget.mobile,
                            ),
                          );
                        }
                      }
                    });

                /*Get.to(
                () => Sms2Page(
                  mobile: userData.userModel.mobileNumber,
                ),
              ).then((value) {
                //callback(route);
              });*/
              }
            }
          } else {
            try {
              final err =
                  (model as RegAPIModel).errorMessages.register[0].toString();
              showAlert(msg: err, isToast: true);
            } catch (e) {
              log(e.toString());
            }
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isDOBOK(dob)) {
      return false;
    }
    return true;
  }

  wsLogin() async {
    APIViewModel().req<LoginAPIModel>(
      context: context,
      apiState: APIState(APIType.login, this.runtimeType, null),
      url: APIAuthCfg.LOGIN_URL,
      param: LoginHelper()
          .getParam(email: widget.email.trim(), pwd: widget.pwd.trim()),
      reqType: ReqType.Post,
      isCookie: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Create profile",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Txt(
                  txt: "Select Date of Birth",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(height: 10),
              DatePickerView(
                cap: null,
                borderWidth: 1.5,
                dt: (dob == '') ? 'Select Date' : dob,
                txtColor: (dob == '') ? Colors.grey : Colors.black,
                initialDate:
                    DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                firstDate:
                    DateTime(dateNow.year - 100, dateNow.month, dateNow.day),
                lastDate:
                    DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        dob =
                            DateFormat('dd-MMM-yyyy').format(value).toString();
                        final dobArr = dob.toString().split('-');
                        this.dobDD = dobArr[0];
                        this.dobMM = dobArr[1];
                        this.dobYY = dobArr[2];
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                },
              ),
              SizedBox(height: 30),
              drawGender(),
              SizedBox(height: 20),
              MMBtn(
                txt: "Get Started",
                bgColor: MyTheme.greenColor,
                txtColor: Colors.white,
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                radius: 0,
                callback: () {
                  if (validate()) {
                    APIViewModel().req<RegAPIModel>(
                      context: context,
                      apiState: APIState(APIType.reg, this.runtimeType, null),
                      url: APIAuthCfg.REG_URL,
                      reqType: ReqType.Post,
                      param: {
                        "Email": widget.email,
                        "Password": widget.pwd,
                        "FirstName": widget.fname,
                        "LastName": widget.lname,
                        "MobileNumber": widget.mobile,
                        "CommunityId": "1",
                        "Persist": false,
                        "CheckSignUpMobileNumber": false,
                        "CountryCode": widget.countryDialCode,
                        "Status": "101",
                        "OTPCode": "",
                        "BirthDay": dobDD,
                        "BirthMonth": dobMM,
                        "BirthYear": dobYY,
                        // "UserCompanyId": 1003,
                        // "UserCompanyId": 2,
                        "UserCompanyId": 1367,
                        "dialCode": widget.countryDialCode,
                        "ConfirmPassword": widget.pwd,
                        "confirmPassword": widget.pwd,
                        "DateofBirth": dob,
                      },
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Txt(
            txt: "Gender",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: true,
          ),
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.black,
              disabledColor: Colors.black,
              selectedRowColor: Colors.black,
              indicatorColor: Colors.black,
              toggleableActiveColor: Colors.black,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.male;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.male,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Male",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.female;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.female,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Female",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
