import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/otp/sms_page1.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/ForgotDialog.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'auth_base.dart';

class LoginPage extends StatefulWidget {
  @override
  State createState() => _LoginPageState();
}

class _LoginPageState extends BaseAuth<LoginPage> with APIStateListener {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pwd = TextEditingController();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.login && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel, false);
        }
      } else if (apiState.type == APIType.forgot &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              final msg = model.messages.forgotpassword[0].toString();
              showAlert(msg: msg, isToast: true, which: 1);
            } catch (e) {
              log(e.toString());
            }
          } else {
            try {
              final err = model.errorMessages.forgotpassword[0].toString();
              showAlert(msg: err, isToast: true);
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel, false);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel, false);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      if (Server.isTest) {
        _email.text =
            "anisur5001@yopmail.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
        _pwd.text = "123456";
        /*
nipunrozario@rocketmail.com
1234
        */
      }
    } catch (e) {}

    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (UserProfileVal().isEmpty(_email, 'Email/Phone')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Login",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 50),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 40, right: 40),
                  child: Column(
                    children: [
                      Txt(
                          txt: "Already User",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize + 1,
                          txtAlign: TextAlign.center,
                          isBold: false),
                      SizedBox(height: 20),
                      InputBox(
                        ctrl: _email,
                        lableTxt: "Email or Phone Number",
                        kbType: TextInputType.emailAddress,
                        len: 50,
                      ),
                      InputBox(
                        ctrl: _pwd,
                        lableTxt: "Password",
                        kbType: TextInputType.text,
                        len: 20,
                        isPwd: true,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                              onTap: () {
                                showForgotDialog(
                                    context: context,
                                    email: _email,
                                    callback: (String emailStr) {
                                      //
                                      if (emailStr != null) {
                                        if (emailStr.length > 0) {
                                          APIViewModel().req<ForgotAPIModel>(
                                            context: context,
                                            apiState: APIState(APIType.forgot,
                                                this.runtimeType, null),
                                            url: APIAuthCfg.FORGOT_URL,
                                            param: {
                                              'email': _email.text.trim()
                                            },
                                            reqType: ReqType.Post,
                                          );
                                        } else {
                                          showAlert(
                                              msg: "Missing email",
                                              isToast: true);
                                        }
                                      }
                                    });
                              },
                              child: Txt(
                                  txt: "Forgot Password ?",
                                  txtColor: Colors.blueAccent,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false)),
                          SizedBox(width: 10),
                          MMBtn(
                            txt: "Login",
                            txtColor: Colors.white,
                            bgColor: MyTheme.greenColor,
                            width: getWP(context, 30),
                            height: getHP(context, MyTheme.btnHpa),
                            radius: 20,
                            callback: () {
                              if (validate()) {
                                APIViewModel().req<LoginAPIModel>(
                                  context: context,
                                  apiState: APIState(
                                      APIType.login, this.runtimeType, null),
                                  url: APIAuthCfg.LOGIN_URL,
                                  param: LoginHelper().getParam(
                                      email: _email.text.trim(),
                                      pwd: _pwd.text.trim()),
                                  reqType: ReqType.Post,
                                  isCookie: true,
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Txt(
                txt: "Or continue with",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40, right: 40, top: 20),
                child: MMBtn(
                  txt: "Log in with Mobile",
                  bgColor: MyTheme.greenColor,
                  txtColor: Colors.white,
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 10,
                  callback: () async {
                    Get.to(
                      () => Sms1Page(),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 40, right: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, MyTheme.btnHpa + 2),
                      width: getW(context),
                      icon: "ic_fb",
                      txt: "Continue with Facebook",
                      callback: () {
                        loginWithFB(this.runtimeType);
                      },
                    ),
                    SizedBox(height: 20),
                    AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, MyTheme.btnHpa + 2),
                      width: getW(context),
                      icon: "ic_google",
                      txt: "Continue with Google",
                      callback: () {
                        loginWithGoogle(this.runtimeType);
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
