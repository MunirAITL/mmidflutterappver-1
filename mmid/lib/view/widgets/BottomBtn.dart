import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/material.dart';
import 'MMBtn.dart';

drawBottomBtn({
  @required context,
  @required String text,
  @required Function callback,
  Color bgColor,
  Color txtColor,
}) {
  if (bgColor == null) {
    bgColor = MyTheme.bgColor;
  }
  if (txtColor == null) {
    txtColor = MyTheme.greyColor;
  }
  double height = MediaQuery.of(context).size.height;
  var padding = MediaQuery.of(context).padding;
  double newheight = height - padding.top - padding.bottom;
  return BottomAppBar(
    color: MyTheme.bgColor,
    child: Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
      child: MMBtn(
        txt: text,
        txtColor: Colors.white,
        bgColor: MyTheme.greyColor,
        //icon: null,
        height: newheight * MyTheme.btnHpa / 100,
        width: MediaQuery.of(context).size.width,
        radius: 10,
        callback: () {
          callback();
        },
      ),
    ),
    elevation: MyTheme.botbarElevation,
  );
}
