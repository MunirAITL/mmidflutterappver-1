import 'package:aitl/config/server/Server.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/utils/mixin.dart';
import 'package:aitl/view/widgets/webview/WebviewScaffoldPage.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

enum eWebType {
  WEB,
  CASE_DETAILS,
  CASE_REVIEW,
  EID,
}

class WebScreen extends StatefulWidget {
  final eWebType ewebType;
  final String url;
  final String title;
  final String caseID;

  const WebScreen({
    Key key,
    this.caseID,
    @required this.ewebType,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> with Mixin {
  //final GlobalKey webViewKey = GlobalKey();
  //InAppWebViewController webView;

  InAppWebViewController wController;
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  final CookieManager cookieManager = CookieManager.instance();

  double progress = 0;
  bool isLoading = false;
  String cookieStr;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    cookieStr = null;
    //webView = null;
    flutterWebviewPlugin.close();
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  appInit() async {
    try {
      /*CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();*/

      CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      log(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Uri.parse(Server.BASE_URL),
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );

        /*flutterWebviewPlugin.onUrlChanged.listen((String url) {
          log("onUrlChanged::" + url);
        });*/

        flutterWebviewPlugin.onStateChanged.listen((viewState) {
          if (viewState.type == WebViewState.startLoad) {
            //EasyLoading.show(status: "Loading...");
            //setState(() {
            //isLoading = true;
            //});
          } else if (viewState.type == WebViewState.finishLoad) {
            // Future.delayed(Duration(seconds: 5), () {
            //EasyLoading.dismiss();
            //});

            //setState(() {
            //isLoading = false;
            //  });
          }
        });

        /*flutterWebviewPlugin.onScrollYChanged.listen((double y) {
          if (mounted) {
            /*setState(() {
              _shouldShowBottomBar = y < 50;
            });*/
          }
        });*/

        setState(() {});
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return (cookieStr == null)
        ? SizedBox()
        : /*InAppWebView(
            initialUrlRequest: URLRequest(url: Uri.parse(widget.url)),
            onWebViewCreated: (controller) {
              wController = controller;
              controller.addJavaScriptHandler(
                handlerName: 'Back',
                callback: (data) {
                  Navigator.of(context).pop();
                },
              );
              controller.addJavaScriptHandler(
                handlerName: 'callbackclientrecommendationagreement',
                callback: (data) {
                  Get.offAll(MainCustomerScreenNew());
                },
              );
              controller.addJavaScriptHandler(
                handlerName: 'callbackclientagreement',
                callback: (data) {
                  Get.offAll(MainCustomerScreenNew());
                },
              );
              controller.addJavaScriptHandler(
                  handlerName: 'Submit_Application',
                  callback: (data) {
                    print("Submit_Application value 2===" + data.toString());
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => AlertDialogCustom(
                              onConfirmClick: () {
                                SubmitApplication submitCaseAPIModel =
                                    new SubmitApplication(
                                        id: 0,
                                        userId: userData.userModel.id,
                                        status: 101,
                                        mortgageCaseInfoId: 0,
                                        taskId: int.parse(widget.caseID),
                                        creationDate: DateTime.now().toString(),
                                        updatedDate: DateTime.now().toString(),
                                        versionNumber: 0,
                                        defineSolution: "",
                                        lenderProvider: "",
                                        term: "",
                                        loanAmount: 0,
                                        rate: 0,
                                        value: 0,
                                        fixedPeriodEnds: "",
                                        procFees: 0,
                                        adviceFee: 0,
                                        introducerFeeShare: 0,
                                        introducerPaymentMade: "",
                                        reasonForRecommendation: "",
                                        anyOtherComments: "",
                                        caseStatus: "",
                                        remarks: "",
                                        monthlyPaymentAmount: 0,
                                        otherChargeAmount: 0,
                                        rentalIncomeAmount: 0,
                                        valuationDate: "",
                                        fileUrl: "");

                                log("name toJson === ${submitCaseAPIModel.toJson()}");
                                log("name encode === ${json.encode(submitCaseAPIModel)}");
                                log("name toString === ${submitCaseAPIModel.toString()}");
                                log("name === ${userData.userModel.name}");

                                SubmitCaseAPIMgr().wsOnPostCase(
                                    context: context,
                                    param: json.encode(submitCaseAPIModel),
                                    callback: (value) {
                                      log("success === ${value.toString()}");
                                      try {
                                        Navigator.of(context).pop();
                                        navTo(
                                                context: context,
                                                page: () =>
                                                    MainCustomerScreenNew())
                                            .then((value) {
                                          print(
                                              "Submit_Application value 2===" +
                                                  value.toString());
                                        });
                                      } catch (e) {
                                        /*log("success === ${value.success}");
                                  log("success Message === ${value.messages}");*/
                                      }
                                    });

                                log("title = ${widget.title} track id = ${widget.caseID}");
                              },
                              onCancelClick: () {
                                Navigator.of(context).pop();
                              },
                            ));
                  });
            },
          );*/

        drawWebView(
            context: context,
            title: widget.title,
            url: widget.url,
            cookieStr: cookieStr,
            ewebType: widget.ewebType,
            callbackTC: (JavascriptMessage message) {});
  }
}
