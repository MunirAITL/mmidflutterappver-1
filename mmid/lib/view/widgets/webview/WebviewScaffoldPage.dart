import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';

drawWebView(
    {BuildContext context,
    String title,
    String url,
    String cookieStr,
    eWebType ewebType,
    Function(JavascriptMessage) callbackTC}) {
  return Scaffold(
    resizeToAvoidBottomInset: true,
    body: WebviewScaffold(
      //resizeToAvoidBottomInset: false,
      resizeToAvoidBottomInset: true,
      scrollBar: true,
      appBar: AppBar(
        elevation: MyTheme.appbarElevation,
        backgroundColor: MyTheme.bgColor,
        automaticallyImplyLeading: false,
        title: Txt(
            txt: title,
            txtColor: Colors.black,
            txtSize: MyTheme.appbarTitleFontSize,
            txtAlign: TextAlign.start,
            isBold: true),
        centerTitle: false,
        actions: [
          IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.close,
                color: MyTheme.greenColor,
                size: 20,
              ))
        ],
      ),
      allowFileURLs: true,
      url: url,
      headers: {'Cookie': cookieStr},
      withLocalStorage: true,
      //enableAppScheme: true,
      hidden: true,
      withZoom: true,
      appCacheEnabled: false,
      debuggingEnabled: true,
      clearCookies: false,
      mediaPlaybackRequiresUserGesture: false,
      ignoreSSLErrors: true,
      //useWideViewPort: false,
      javascriptChannels: [
        JavascriptChannel(
            name: 'Back',
            onMessageReceived: (JavascriptMessage message) {
              print(message.message);
              Navigator.of(context).pop();
            }),
      ].toSet(),

      withJavascript: true,
      initialChild: Container(
        color: Colors.white,
        child: Center(
          child: CircularProgressIndicator(
            color: MyTheme.brandColor,
          ),
        ),
      ),
    ),
  );
}
