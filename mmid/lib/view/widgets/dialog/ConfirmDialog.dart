import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Btn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmDialog extends StatelessWidget {
  final title, msg;
  final Function callback;
  const ConfirmDialog({
    Key key,
    @required this.callback,
    @required this.title,
    @required this.msg,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .2,
                txtAlign: TextAlign.center,
                isBold: false,
                //txtLineSpace: 1.5,
              ),
              SizedBox(height: 10),
              Txt(
                txt: msg,
                txtColor: Colors.black87,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
                //txtLineSpace: 1.5,
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Btn(
                    txt: "No",
                    txtColor: Colors.black,
                    bgColor: MyTheme.greyColor,
                    callback: () {
                      Get.back();
                    },
                  ),
                  Btn(
                    txt: "Yes",
                    txtColor: Colors.black,
                    bgColor: MyTheme.greyColor,
                    callback: () {
                      callback();
                      Get.back();
                    },
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
