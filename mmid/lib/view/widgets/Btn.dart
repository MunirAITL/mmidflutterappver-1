import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';

class Btn extends StatelessWidget {
  final String txt;
  final Color txtColor;
  final Color bgColor;
  Color borderColor;
  double txtSize;
  final double radius;
  final Function callback;
  bool isTxtBold;
  Btn({
    Key key,
    @required this.txt,
    @required this.txtColor,
    this.txtSize,
    @required this.bgColor,
    this.borderColor = Colors.transparent,
    this.radius = 10,
    this.isTxtBold = true,
    @required this.callback,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (txtSize == null) txtSize = MyTheme.txtSize;
    return Container(
      //color: Colors.brown,

      child: MaterialButton(
        child: Txt(
          txt: txt,
          txtColor: txtColor,
          txtSize: txtSize,
          txtAlign: TextAlign.center,
          isBold: isTxtBold,
        ),
        onPressed: () => callback(),
        //textColor: Colors.black,
        color: bgColor,
        shape: OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid, width: .5, color: this.borderColor),
            borderRadius: new BorderRadius.circular(radius)),
      ),
    );
  }
}
