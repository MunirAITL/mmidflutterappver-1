import 'package:flutter/material.dart';

class BadgeIcon extends StatelessWidget {
  final Widget iconWidget;
  final String text;
  final int notificationCount;

  const BadgeIcon({
    Key key,
    @required this.text,
    @required this.iconWidget,
    this.notificationCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 72,
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              iconWidget,
              Text(text, overflow: TextOverflow.ellipsis),
            ],
          ),
          Positioned(
            top: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 2),
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.red),
              alignment: Alignment.center,
              child: Text('$notificationCount'),
            ),
          )
        ],
      ),
    );
  }
}
