class PostDocByBase64BitAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostDocByBase64BitAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostDocByBase64BitAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  Responsedata responsedata;

  ResponseData({this.responsedata});

  ResponseData.fromJson(Map<String, dynamic> json) {
    responsedata = json['responsedata'] != null
        ? new Responsedata.fromJson(json['responsedata'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.responsedata != null) {
      data['responsedata'] = this.responsedata.toJson();
    }
    return data;
  }
}

class Responsedata {
  List<OrganizeOcrData> organizeOcrData;
  List<Imageattribute> imageattribute;

  Responsedata({this.organizeOcrData, this.imageattribute});

  Responsedata.fromJson(Map<String, dynamic> json) {
    if (json['organizeOcrData'] != null) {
      organizeOcrData = [];
      json['organizeOcrData'].forEach((v) {
        organizeOcrData.add(new OrganizeOcrData.fromJson(v));
      });
    }
    if (json['imageattribute'] != null) {
      imageattribute = [];
      json['imageattribute'].forEach((v) {
        imageattribute.add(new Imageattribute.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.organizeOcrData != null) {
      data['organizeOcrData'] =
          this.organizeOcrData.map((v) => v.toJson()).toList();
    }
    if (this.imageattribute != null) {
      data['imageattribute'] =
          this.imageattribute.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrganizeOcrData {
  String item1;
  String item2;

  OrganizeOcrData({this.item1, this.item2});

  OrganizeOcrData.fromJson(Map<String, dynamic> json) {
    item1 = json['Item1'];
    item2 = json['Item2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Item1'] = this.item1;
    data['Item2'] = this.item2;
    return data;
  }
}

class Imageattribute {
  String faceId;
  FaceRectangle faceRectangle;
  FaceAttributes faceAttributes;

  Imageattribute({this.faceId, this.faceRectangle, this.faceAttributes});

  Imageattribute.fromJson(Map<String, dynamic> json) {
    faceId = json['faceId'];
    faceRectangle = json['faceRectangle'] != null
        ? new FaceRectangle.fromJson(json['faceRectangle'])
        : null;
    faceAttributes = json['faceAttributes'] != null
        ? new FaceAttributes.fromJson(json['faceAttributes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['faceId'] = this.faceId;
    if (this.faceRectangle != null) {
      data['faceRectangle'] = this.faceRectangle.toJson();
    }
    if (this.faceAttributes != null) {
      data['faceAttributes'] = this.faceAttributes.toJson();
    }
    return data;
  }
}

class FaceRectangle {
  int top;
  int left;
  int width;
  int height;

  FaceRectangle({this.top, this.left, this.width, this.height});

  FaceRectangle.fromJson(Map<String, dynamic> json) {
    top = json['top'];
    left = json['left'];
    width = json['width'];
    height = json['height'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['top'] = this.top;
    data['left'] = this.left;
    data['width'] = this.width;
    data['height'] = this.height;
    return data;
  }
}

class FaceAttributes {
  double smile;
  HeadPose headPose;
  String gender;
  dynamic age;
  FacialHair facialHair;
  String glasses;
  Emotion emotion;
  Blur blur;
  Exposure exposure;
  Noise noise;
  Makeup makeup;
  List<Accessories> accessories;
  Occlusion occlusion;
  Hair hair;

  FaceAttributes(
      {this.smile,
      this.headPose,
      this.gender,
      this.age,
      this.facialHair,
      this.glasses,
      this.emotion,
      this.blur,
      this.exposure,
      this.noise,
      this.makeup,
      this.accessories,
      this.occlusion,
      this.hair});

  FaceAttributes.fromJson(Map<String, dynamic> json) {
    smile = json['smile'];
    headPose = json['headPose'] != null
        ? new HeadPose.fromJson(json['headPose'])
        : null;
    gender = json['gender'];
    age = json['age'];
    facialHair = json['facialHair'] != null
        ? new FacialHair.fromJson(json['facialHair'])
        : null;
    glasses = json['glasses'];
    emotion =
        json['emotion'] != null ? new Emotion.fromJson(json['emotion']) : null;
    blur = json['blur'] != null ? new Blur.fromJson(json['blur']) : null;
    exposure = json['exposure'] != null
        ? new Exposure.fromJson(json['exposure'])
        : null;
    noise = json['noise'] != null ? new Noise.fromJson(json['noise']) : null;
    makeup =
        json['makeup'] != null ? new Makeup.fromJson(json['makeup']) : null;
    if (json['accessories'] != null) {
      accessories = [];
      json['accessories'].forEach((v) {
        accessories.add(new Accessories.fromJson(v));
      });
    }
    occlusion = json['occlusion'] != null
        ? new Occlusion.fromJson(json['occlusion'])
        : null;
    hair = json['hair'] != null ? new Hair.fromJson(json['hair']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['smile'] = this.smile;
    if (this.headPose != null) {
      data['headPose'] = this.headPose.toJson();
    }
    data['gender'] = this.gender;
    data['age'] = this.age;
    if (this.facialHair != null) {
      data['facialHair'] = this.facialHair.toJson();
    }
    data['glasses'] = this.glasses;
    if (this.emotion != null) {
      data['emotion'] = this.emotion.toJson();
    }
    if (this.blur != null) {
      data['blur'] = this.blur.toJson();
    }
    if (this.exposure != null) {
      data['exposure'] = this.exposure.toJson();
    }
    if (this.noise != null) {
      data['noise'] = this.noise.toJson();
    }
    if (this.makeup != null) {
      data['makeup'] = this.makeup.toJson();
    }
    if (this.accessories != null) {
      data['accessories'] = this.accessories.map((v) => v.toJson()).toList();
    }
    if (this.occlusion != null) {
      data['occlusion'] = this.occlusion.toJson();
    }
    if (this.hair != null) {
      data['hair'] = this.hair.toJson();
    }
    return data;
  }
}

class HeadPose {
  double pitch;
  double roll;
  double yaw;

  HeadPose({this.pitch, this.roll, this.yaw});

  HeadPose.fromJson(Map<String, dynamic> json) {
    pitch = json['pitch'];
    roll = json['roll'];
    yaw = json['yaw'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pitch'] = this.pitch;
    data['roll'] = this.roll;
    data['yaw'] = this.yaw;
    return data;
  }
}

class FacialHair {
  double moustache;
  double beard;
  double sideburns;

  FacialHair({this.moustache, this.beard, this.sideburns});

  FacialHair.fromJson(Map<String, dynamic> json) {
    moustache = json['moustache'];
    beard = json['beard'];
    sideburns = json['sideburns'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['moustache'] = this.moustache;
    data['beard'] = this.beard;
    data['sideburns'] = this.sideburns;
    return data;
  }
}

class Emotion {
  double anger;
  double contempt;
  double disgust;
  double fear;
  double happiness;
  double neutral;
  double sadness;
  double surprise;

  Emotion(
      {this.anger,
      this.contempt,
      this.disgust,
      this.fear,
      this.happiness,
      this.neutral,
      this.sadness,
      this.surprise});

  Emotion.fromJson(Map<String, dynamic> json) {
    anger = json['anger'];
    contempt = json['contempt'];
    disgust = json['disgust'];
    fear = json['fear'];
    happiness = json['happiness'];
    neutral = json['neutral'];
    sadness = json['sadness'];
    surprise = json['surprise'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['anger'] = this.anger;
    data['contempt'] = this.contempt;
    data['disgust'] = this.disgust;
    data['fear'] = this.fear;
    data['happiness'] = this.happiness;
    data['neutral'] = this.neutral;
    data['sadness'] = this.sadness;
    data['surprise'] = this.surprise;
    return data;
  }
}

class Blur {
  String blurLevel;
  double value;

  Blur({this.blurLevel, this.value});

  Blur.fromJson(Map<String, dynamic> json) {
    blurLevel = json['blurLevel'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['blurLevel'] = this.blurLevel;
    data['value'] = this.value;
    return data;
  }
}

class Exposure {
  String exposureLevel;
  double value;

  Exposure({this.exposureLevel, this.value});

  Exposure.fromJson(Map<String, dynamic> json) {
    exposureLevel = json['exposureLevel'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['exposureLevel'] = this.exposureLevel;
    data['value'] = this.value;
    return data;
  }
}

class Noise {
  String noiseLevel;
  double value;

  Noise({this.noiseLevel, this.value});

  Noise.fromJson(Map<String, dynamic> json) {
    noiseLevel = json['noiseLevel'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['noiseLevel'] = this.noiseLevel;
    data['value'] = this.value;
    return data;
  }
}

class Makeup {
  bool eyeMakeup;
  bool lipMakeup;

  Makeup({this.eyeMakeup, this.lipMakeup});

  Makeup.fromJson(Map<String, dynamic> json) {
    eyeMakeup = json['eyeMakeup'];
    lipMakeup = json['lipMakeup'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eyeMakeup'] = this.eyeMakeup;
    data['lipMakeup'] = this.lipMakeup;
    return data;
  }
}

class Accessories {
  String type;
  double confidence;

  Accessories({this.type, this.confidence});

  Accessories.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    confidence = json['confidence'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['confidence'] = this.confidence;
    return data;
  }
}

class Occlusion {
  bool foreheadOccluded;
  bool eyeOccluded;
  bool mouthOccluded;

  Occlusion({this.foreheadOccluded, this.eyeOccluded, this.mouthOccluded});

  Occlusion.fromJson(Map<String, dynamic> json) {
    foreheadOccluded = json['foreheadOccluded'];
    eyeOccluded = json['eyeOccluded'];
    mouthOccluded = json['mouthOccluded'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['foreheadOccluded'] = this.foreheadOccluded;
    data['eyeOccluded'] = this.eyeOccluded;
    data['mouthOccluded'] = this.mouthOccluded;
    return data;
  }
}

class Hair {
  double bald;
  bool invisible;
  List<HairColor> hairColor;

  Hair({this.bald, this.invisible, this.hairColor});

  Hair.fromJson(Map<String, dynamic> json) {
    bald = json['bald'];
    invisible = json['invisible'];
    if (json['hairColor'] != null) {
      hairColor = [];
      json['hairColor'].forEach((v) {
        hairColor.add(new HairColor.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bald'] = this.bald;
    data['invisible'] = this.invisible;
    if (this.hairColor != null) {
      data['hairColor'] = this.hairColor.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HairColor {
  String color;
  double confidence;

  HairColor({this.color, this.confidence});

  HairColor.fromJson(Map<String, dynamic> json) {
    color = json['color'];
    confidence = json['confidence'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['color'] = this.color;
    data['confidence'] = this.confidence;
    return data;
  }
}
