import 'package:aitl/view/utils/mixin.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefMgr with Mixin {
  static final PrefMgr shared = PrefMgr._internal();
  factory PrefMgr() {
    return shared;
  }

  PrefMgr._internal();

  //  String
  setPrefStr(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(key, val);
    } catch (e) {}
  }

  Future<String> getPrefStr(key) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString(key);
    } catch (e) {}
    return null;
  }

  //  Bool
  setPrefBool(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool(key, val);
    } catch (e) {}
  }

  Future<bool> getPrefBool(key) async {
    bool isOk = false;
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      isOk = prefs.getBool(key);
    } catch (e) {
      log(e.toString());
    }
    return isOk;
  }
}
