import 'dart:io';

class AppData {
  static final AppData _appData = new AppData._internal();

  factory AppData() {
    return _appData;
  }
  AppData._internal();

  File file_selfie;

  File file_drvlic_front;

  File file_pp_front;

  File file_eu_front;
  File file_eu_back;
}

final appData = AppData();
