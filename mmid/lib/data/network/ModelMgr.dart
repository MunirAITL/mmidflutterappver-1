import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/SendOtpNotiAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/media_upload/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/data/model/media_upload/PostDocVerifyAPIModel.dart';
import 'package:aitl/data/model/media_upload/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';

enum APIType {
  login,
  loginWithG,
  loginWithFB,
  forgot,
  reg,
  reg_fb_mobile,
  otp_post,
  otp_put,
}

class APIState {
  APIType type;
  dynamic cls;
  dynamic data;
  APIState(type, cls, data) {
    this.type = type;
    this.cls = cls;
    this.data = data;
  }
}

class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  ///
  /*List<T> fromJsonList<T>(List<dynamic> jsonList) {
    return jsonList
        ?.map<T>((dynamic json) => fromJson<T, void>(json))
        ?.toList();
  }*/

  Future<T> fromJson<T, K>(dynamic json) async {
    //return fromJsonList<K>(json) as T;

    if (identical(T, CommonAPIModel)) {
      return CommonAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, RegProfileAPIModel)) {
      return RegProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, PostSelfieByBase64DataAPIModel)) {
      return PostSelfieByBase64DataAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocByBase64BitAPIModel)) {
      return PostDocByBase64BitAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocVerifyAPIModel)) {
      return PostDocVerifyAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
