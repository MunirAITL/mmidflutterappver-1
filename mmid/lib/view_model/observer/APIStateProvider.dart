//Singleton reusable class
//  https://medium.com/@nshansundar/simple-observer-pattern-to-notify-changes-across-screens-in-flutter-dart-f035dbd990a9
import 'package:aitl/data/network/ModelMgr.dart';

abstract class APIStateListener<T> {
  void onAPIStateChanged(APIState apiState, T model);
}

enum APIObserverState {
  STATE_DONE,
}

class APIStateProvider {
  List<APIStateListener> observers = [];
  static final APIStateProvider _instance = new APIStateProvider.internal();
  factory APIStateProvider() => _instance;
  APIStateProvider.internal() {
    //observers = new List<StateListener>();
    initState();
  }
  void initState() async {}

  void subscribe(APIStateListener listener) {
    observers.add(listener);
  }

  void unsubscribe(APIStateListener listener) {
    observers.remove(listener);
  }

  void notify<T>(dynamic state, APIState apiState, T model) {
    observers.forEach(
        (APIStateListener obj) => obj.onAPIStateChanged(apiState, model));
  }

  void dispose(APIStateListener thisObserver) {
    for (var obj in observers) {
      if (obj == thisObserver) {
        observers.remove(obj);
      }
    }
  }
}
