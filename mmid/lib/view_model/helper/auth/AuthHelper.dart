import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class AuthHelper {
  drawGoogleFBLoginButtons(
      {BuildContext context,
      String icon,
      String txt,
      double height,
      double width,
      Function callback}) {
    return Container(
      height: height,
      width: width,
      child: OutlinedButton(
        onPressed: () {
          callback();
        },
        style: ButtonStyle(
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          ),
          side: MaterialStateProperty.resolveWith((states) {
            return BorderSide(color: MyTheme.greenColor, width: 1);
          }),
        ),
        child: ListTile(
          leading: SvgPicture.asset(
            'assets/images/svg/' + icon + '.svg',
            //fit: BoxFit.fill,
          ),
          title: Txt(
              txt: txt,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          trailing: SizedBox(),
        ),
      ),
    );
  }
}
