import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:aitl/config/server/APIMediaCfg.dart';
import 'package:aitl/data/model/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/rx/UploadProgController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';
import 'package:synchronized/synchronized.dart';

class APIViewModel<T> {
  Future<void> req<T>({
    @required BuildContext context,
    APIState apiState,
    @required String url,
    @required ReqType reqType,
    Map<String, dynamic> param,
    headers,
    bool isLoading = true,
    bool isCookie = false,
    Function(T) callback,
  }) async {
    try {
      final progController = Get.put(UploadProgController());
      var lock = new Lock();
      await lock.synchronized(() async {
        final jsonString = JsonString(json.encode(param));
        log(jsonString.source);
        await NetworkMgr.shared
            .req<T, Null>(
          context: context,
          url: url,
          param: param,
          reqType: reqType,
          headers: headers,
          isLoading: isLoading,
          isCookie: isCookie,
          progController: progController,
        )
            .then((model) async {
          lock = null;
          if (apiState != null) {
            APIStateProvider()
                .notify(APIObserverState.STATE_DONE, apiState, model);
          } else {
            if (callback != null) callback(model);
          }
        });
      });
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> upload({
    context,
    APIState apiState,
    File file,
    Function(MediaUploadFilesAPIModel) callback,
  }) async {
    try {
      final progController = Get.put(UploadProgController());
      var lock = new Lock();
      await lock.synchronized(() async {
        await Future.wait([
          NetworkMgr.shared
              .uploadFiles<MediaUploadFilesAPIModel, Null>(
            context: context,
            url: "", //APIMediaCfg.SELFIE_URL,
            files: [file],
            progController: progController,
          )
              .then((model) async {
            lock = null;
            if (apiState != null) {
              APIStateProvider()
                  .notify(APIObserverState.STATE_DONE, apiState, model);
            } else {
              if (callback != null) callback(model);
            }
          })
        ]);
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
