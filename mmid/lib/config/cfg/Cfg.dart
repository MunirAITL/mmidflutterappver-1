import 'package:aitl/config/server/Server.dart';

class Cfg {
  static const MISSING_IMG =
      Server.BASE_URL + "/api/content/media/default_avatar.png";

  static const TC_URL = "https://www.mortgagemagic.com/privacy-policy/";
  static const PRIVACY_URL = Server.BASE_URL + "/assets/img/privacy_policy.pdf";
}
