import 'package:flutter/material.dart';
import 'package:aitl/view/utils/mixin.dart';

class MyTheme {
  static final double appbarTitleFontSize = 2;
  static final double txtSize = 1.8;
  static final double btnHpa = 6;
  static final double switchBtnHpa = 6;
  static final String fontFamily = "sans";
  static final double appbarElevation = 0;
  static final double botbarElevation = 1;
  static final double txtLineSpace = 1.3;

  static final Color bgColor = HexColor.fromHex("#E0E0E0");
  static final Color brandColor = HexColor.fromHex("#F1F1F4");
  static final Color radioColor = HexColor.fromHex("#F1F1F4");

  static final Color greyColor = HexColor.fromHex("#F1F1F4");
  static final Color lgreyColor = HexColor.fromHex("#E0E0E0");
  static final Color dgreyColor = HexColor.fromHex("#FFA9AC");

  static final Color greenColor = HexColor.fromHex("#009933");

  static final Color lBlueColor = HexColor.fromHex("#f5f9fb");
  static final Color l2BlueColor = HexColor.fromHex("#e7eef1");
  static final Color l3BlueColor = HexColor.fromHex("#cad7dc");

  static final radioThemeData = ThemeData(
    unselectedWidgetColor: MyTheme.radioColor,
    disabledColor: MyTheme.radioColor,
    selectedRowColor: MyTheme.radioColor,
    indicatorColor: MyTheme.radioColor,
    toggleableActiveColor: MyTheme.radioColor,
  );

  static final ThemeData themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.white,
    accentColor: HexColor.fromHex("#ffffff"),
    //iconTheme: IconThemeData(color: Colors.red),
    //primarySwatch: Colors.grey,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    //visualDensity: VisualDensity.adaptivePlatformDensity,
    //scaffoldBackgroundColor: Colors.pink,

    cardTheme: CardTheme(
      color: Colors.white,
    ),

    //primarySwatch: Colors.white,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),

    /*inputDecorationTheme: InputDecorationTheme(
      focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),*/

    // Define the default font family.
    fontFamily: fontFamily,

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
      headline6: TextStyle(
          fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontFamily: fontFamily, color: Colors.black),
    ),

    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blueAccent,
      shape: RoundedRectangleBorder(),
      textTheme: ButtonTextTheme.accent,
      splashColor: Colors.lime,
    ),*/
  );

  static final boxDeco = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      border: Border(
          left: BorderSide(color: Colors.grey, width: 1),
          right: BorderSide(color: Colors.grey, width: 1),
          top: BorderSide(color: Colors.grey, width: 1),
          bottom: BorderSide(color: Colors.grey, width: 1)),
      color: Colors.transparent);
}
