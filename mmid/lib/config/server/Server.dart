class Server {
  static const bool isTest = true;

  //  BaseUrl
  static const String BASE_IP = "158.176.138.61";
  static const String BASE_URL = (isTest)
      ? "https://app.mortgage-magic.co.uk"
      : "https://mortgage-magic.co.uk";

  static const String WSAPIKEY = "ABCXYZ123TEST";

  static const String EID_URL = BASE_URL + "/mrg/mark-eid-mobile-verification";
}
