import 'package:aitl/config/server/Server.dart';

class APIMediaCfg {
  static const SELFIE_POST_URL =
      Server.BASE_URL + "/api/camcapture/postselfieBydase64data";
  static const DOC_POST_URL =
      Server.BASE_URL + "/api/camcapture/postdocumentbybase64bitdata";
  static const VERIFY_POST_URL =
      Server.BASE_URL + "/api/camcapture/postdocumentverify";
}
